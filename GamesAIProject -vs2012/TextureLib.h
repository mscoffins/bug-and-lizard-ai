#pragma once;
#include "glew.h"
#include <windows.h>		// Header file for Windows
#include <gl\gl.h>			// Header file for the OpenGL32 Library
#include <gl\glu.h>			// Header file for the GLu32 Library
#include "General.h"
#include "nvImage.h"

class TextureLib{
public:
	TextureLib();
	void init();
	GLuint loadPNG(char* name);

	vector<GLuint> bugtexsM;
	vector<GLuint> liztexsM;
	vector<GLuint> bugtexsF;
	vector<GLuint> liztexsF;
	vector<GLuint> bugHomeTexs;
	vector<GLuint> lizHomeTexs;
	GLuint foodtex;
	GLuint background;
	GLuint statsBG;
	vector<GLuint> wallTexs;
};