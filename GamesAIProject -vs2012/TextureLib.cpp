#include "TextureLib.h"

TextureLib::TextureLib(){
	bugtexsM = vector<GLuint>();
	liztexsM = vector<GLuint>();
	bugtexsF = vector<GLuint>();
	liztexsF = vector<GLuint>();
	bugHomeTexs = vector<GLuint>();
	lizHomeTexs = vector<GLuint>();
	wallTexs = vector<GLuint>();
}

GLuint TextureLib::loadPNG(char* name){
	// Texture loading object
	nv::Image img;

	GLuint myTextureID;

	// Return true on success
	if(img.loadImageFromFile(name))
	{
		glGenTextures(1, &myTextureID);
		glBindTexture(GL_TEXTURE_2D, myTextureID);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	}

	else
		MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);

	return myTextureID;
}

void TextureLib::init(){
	GLuint grass,foodTex, bugHome, lizHome;
	grass = loadPNG("textures/grass.png");
	foodTex = loadPNG("textures/dirt2.png");
	bugHome = loadPNG("textures/bugHome.png");
	lizHome = loadPNG("textures/LizHome.png");
	statsBG = loadPNG("textures/statsBG.png");
	bugtexsM.push_back(loadPNG("textures/bugM.png"));
	liztexsM.push_back(loadPNG("textures/lizard00M.png"));
	liztexsM.push_back(loadPNG("textures/lizard01M.png"));
	liztexsM.push_back(loadPNG("textures/lizard1M.png"));
	liztexsM.push_back(loadPNG("textures/lizard2M.png"));
	liztexsM.push_back(loadPNG("textures/lizard3M.png"));
	liztexsM.push_back(loadPNG("textures/lizard2M.png"));
	liztexsM.push_back(loadPNG("textures/lizard1M.png"));
	liztexsM.push_back(loadPNG("textures/lizard01M.png"));
	bugtexsF.push_back(loadPNG("textures/bugF.png"));
	liztexsF.push_back(loadPNG("textures/lizard00F.png"));
	liztexsF.push_back(loadPNG("textures/lizard01F.png"));
	liztexsF.push_back(loadPNG("textures/lizard1F.png"));
	liztexsF.push_back(loadPNG("textures/lizard2F.png"));
	liztexsF.push_back(loadPNG("textures/lizard3F.png"));
	liztexsF.push_back(loadPNG("textures/lizard2F.png"));
	liztexsF.push_back(loadPNG("textures/lizard1F.png"));
	liztexsF.push_back(loadPNG("textures/lizard01F.png"));
	bugHomeTexs.push_back(bugHome);
	lizHomeTexs.push_back(lizHome);
	background = grass;
	foodtex = foodTex;
	wallTexs.push_back(loadPNG("textures/wall.png"));

}