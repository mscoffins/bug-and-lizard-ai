#pragma once
const int MAP_WIDTH = 30;
const int MAP_HEIGHT = 24;

enum nodeStatus{
	UNEXPLORED, 
	OPENLIST,
	CLOSEDLIST,
	INVALID,
	VALID
};

class AStarNode{
public:
	AStarNode(int x = 0, int y = 0, nodeStatus s = UNEXPLORED, int w = 1){
		this->x = x;
		this->y = y;
		id = x * MAP_WIDTH + y;
		numChildren = 0;
		f = g = h = 0;
		status = s;
		weight = w;
	}

	bool operator<(const AStarNode* node)const{ return this->f < node->f;}

	int f, g, h;
	int x, y;
	int numChildren;
	int id;
	int weight;
	AStarNode* parent;
	AStarNode* next;
	AStarNode* children[8];
	nodeStatus status;
};

