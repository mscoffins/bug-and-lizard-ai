#pragma once
using namespace std;
#include "Point2d.h"
#include "glew.h"
#include <vector>
class Drawable {
private:
	float width;
	float height;
	Point2d centre;
	vector<GLuint>* textures;
	GLuint currentTex;
	float count;

public:

	Drawable(float w = 1.0f, float h = 1.0f, Point2d c = Point2d(0, 0), vector<GLuint>* t = new vector<GLuint>());
	void draw(float angle);
	void init();
	void update(float deltaT);
	Point2d getCentre() { return this->centre; }
	float getHeight(){ return height; }
	float getWidth(){ return width; }
	void setCentre(Point2d c) { centre = c; }
	void setWidth(float w){ width = w; }
	void setHeight(float h){ height = h; }

};
