#include "FSMStateLizardMate.h"

FSMStateLizardMate::FSMStateLizardMate(int state, AIActor* p)
{
	this->type = state;
	this->parent = p;
	mateDur = 0;
}

void FSMStateLizardMate::update(float deltaT)
{
	mateDur += deltaT;
}

void FSMStateLizardMate::enter()
{
	mateDur = 0;
	AIActorLizard* liz = (AIActorLizard*)parent;
	if(liz->nearestLizard != NULL){
		AIActorLizard* mate = liz->nearestLizard;
		liz->setSpeed(0);
		mate->setSpeed(0);
		mate->setBreeding(true);
		liz->setBreeding(true);
	}
}

void FSMStateLizardMate::exit()
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	if(liz->nearestLizard != NULL){
		AIActorLizard* mate = liz->nearestLizard;
		mate->setSpeed(mate->baseSpeed);
		mate->setBreeding(false);
		mate->setStamina(mate->getStamina() - 20);
		mate->setHunger(mate->getHunger() + 50);
		liz->setSpeed(liz->baseSpeed);
		liz->setBreeding(false);
		liz->setStamina(liz->getStamina() - 20);
		liz->setHunger(liz->getHunger() + 50);

		if(mateDur > 2){
			liz->childFlag = true;
			liz->setBreedingDelay(liz->baseBreedingDelay);
			mate->setBreedingDelay(mate->baseBreedingDelay);
		}
	}
	liz->updatePercep = true;
}

void FSMStateLizardMate::init()
{
}

int FSMStateLizardMate::checkTransitions() {
	AIActorLizard* liz = (AIActorLizard*)parent;

	if(mateDur >= 2 || liz->nearestLizard->getBreedingDelay() > 0)
		return FSM_STATE_LIZARD_WANDER;

	return type;
}