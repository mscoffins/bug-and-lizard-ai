#include "Drawable.h"

Drawable::Drawable(float w, float h, Point2d c, vector<GLuint>* t){

	width = w;
	height = h;
	centre = c;
	textures = t;
	count = 0.001;
}

void Drawable::draw(float angle){
	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, currentTex);//set texture
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);//dont repeat

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);//use aplha channel
	glTranslatef(centre.x,centre.y,0);
	glRotatef(angle-90,0,0,1);
	//draw object
	glBegin(GL_POLYGON);
	glTexCoord2f( 0.0 , 1.0 ); glVertex2f(- width / 2, + height / 2 );
	glTexCoord2f( 0.0 , 0.0 ); glVertex2f(- width / 2, - height / 2 );
	glTexCoord2f( 1.0 , 0.0 ); glVertex2f( width / 2, - height / 2 );
	glTexCoord2f( 1.0 , 1.0 ); glVertex2f( width / 2, height / 2 );
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glPopMatrix();

}

void Drawable::init(){
	currentTex = textures->at(0);
}

void Drawable::update(float deltaT){
	
	if(textures->size()>1){
		count += deltaT;
		if(count >= textures->size())
			count = 0.01;
		currentTex = textures->at(ceilf(count) - 1);
	}

}
