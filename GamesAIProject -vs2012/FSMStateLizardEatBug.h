#pragma once
#include "FSMState.h"
#include "AIActor.h"
#include "AIActorLizard.h"

class FSMStateLizardEatBug : public FSMState{
private: 
	float eatDur;
	float lizSpeed;
	float bugSpeed;
public:
	FSMStateLizardEatBug(int state, AIActor* p);

	virtual void update(float deltaT);
	virtual void enter();
	virtual void exit();
	virtual void init();
	virtual int checkTransitions();
};