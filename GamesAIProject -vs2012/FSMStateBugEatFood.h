#pragma once
#include "FSMState.h"
#include "AIActor.h"
#include "AIActorBug.h"
#include <random>
#include <time.h>

class FSMStateBugEatFood : public FSMState{
private:
	float bugSpeed;
	Point2d foodPos;
public:
	FSMStateBugEatFood(int s, AIActor* parent);
	virtual void update(float deltaT);
	virtual void enter();
	virtual void exit();
	virtual void init();
	virtual int checkTransitions();
};