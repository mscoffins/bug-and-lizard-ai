#pragma once

#include "FSMState.h"
#include "AIActor.h"
#include "AIActorLizard.h"
#include <random>
#include <time.h>

class FSMStateLizardRest : public FSMState{
private:

public:
	FSMStateLizardRest(int state, AIActor* p);

	virtual void update(float deltaT);
	virtual void enter();
	virtual void exit();
	virtual void init();
	virtual int checkTransitions();
};