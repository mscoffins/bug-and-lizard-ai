#pragma once
#include "General.h"
#include "Food.h"
#include "AIActor.h"
#include "States.h"

class AIActorLizard;

class AIActorBug : public AIActor {
private:
	float caughtDelay;
	float breedDelayReset;
public:
	static int CurrentID;

	AIActorBug() {};
	AIActorBug(Drawable* bug, Gender s, int* map);
	AIActorBug(Drawable* bug, Gender s, int* map, AIProperties p);
	virtual void draw();
	virtual void update(float deltaT);
	virtual void init();
	void updatePerceptions(AIActorLizard* nearLiz, AIActorBug* nearBug, Food* f, Home* nearHome);
	void spawnAtHome();


	
	void kill(){alive = false;}
	bool isCaught(){return caught;}
	void setCaught(){caught = true;}

	
	bool caught;
	AIActorBug* nearestBug;
	AIActorLizard* nearestLiz;
	Food* nearestFood;

	float PercepConf;

	~AIActorBug(){
		delete brain;
	}
};