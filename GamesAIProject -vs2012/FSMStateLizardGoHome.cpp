#include "FSMStateLizardGoHome.h"

FSMStateLizardGoHome::FSMStateLizardGoHome(int state, AIActor* p)
{
	this->type = state;
	this->parent = p;
	this->resetDelay = 0;
}

void FSMStateLizardGoHome::update(float deltaT)
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	Point2d pos = parent->getDrawable()->getCentre();
	float px,py;
	int lx, ly;
	px = path.back().y * cellsize - worldW/2 + cellsize/2;
	py = path.back().x * cellsize - worldH/2 + cellsize/2;
	lx = (pos.x + worldW/2)/cellsize;
	ly = (pos.y + worldH/2)/cellsize;
	float angle =  (atan2(py - pos.y,px - pos.x) * 180) / PI;
	//float bAngle = liz->getAngle();
	//float angleInc = (angle - bAngle);
	//if(sq(angle - bAngle) > 100)
	//	liz->setAngle(liz->getAngle() + angleInc);
	//else
		liz->setAngle(angle);
	
	//if(sq(pos.x - px) < 16 && sq(pos.y - py) < 16 && path.size() > 1){
	if(path.back().y == lx && path.back().x == ly && path.size() > 1){
		path.pop_back();
	}
}

void FSMStateLizardGoHome::enter()
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	Point2d pos = parent->getDrawable()->getCentre();
	Point2d homepos = liz->nearestHome->getDrawable()->getCentre();

	path.clear();
	int x = ((int)pos.x + worldW/2)/cellsize;
	int y = ((int)pos.y + worldH/2)/cellsize;
	int tx = (homepos.x + worldW/2)/cellsize;
	int ty = (homepos.y + worldH/2)/cellsize;
	if(x != tx || y != ty){
		AStar pathfinder(parent->map);
		path = pathfinder.search(Point2d(y,x),Point2d(ty,tx));
	}else{
		
		path.push_back(Point2d(y,x));
	}

	//parent->setAngle(180/3.141f * atan2f(homepos.y - lizpos.y,homepos.x - lizpos.x));
	parent->path = &path;
}

void FSMStateLizardGoHome::exit()
{
	path = vector<Point2d>();
	parent->updatePercep = true;
	parent->path = NULL;
}

void FSMStateLizardGoHome::init()
{
	path = vector<Point2d>();
}

int FSMStateLizardGoHome::checkTransitions() {
	AIActorLizard* liz = (AIActorLizard*)parent;
	Point2d lizPos = parent->getDrawable()->getCentre();
	Point2d homePos = liz->nearestHome->getDrawable()->getCentre();

	if(liz->nearestBug != NULL){
		Point2d bugpos = liz->nearestBug->getDrawable()->getCentre();
		float lizr = parent->getDrawable()->getHeight();
		float db = bugpos.dist(lizPos);
		if(liz->getHunger() >= 300 && db <= 2*(lizr * lizr))
			return FSM_STATE_LIZARD_CHASE_BUG;
	}else
		liz->updatePercep = true;

	if(liz->getStamina() < 25 && liz->awayRests < 2)
		return FSM_STATE_LIZARD_REST_AWAY;

	float dh = homePos.dist(lizPos);
	if(dh < 50)
		return FSM_STATE_LIZARD_REST;

	return type;
}