#pragma once
#include "General.h"
#include "AIActor.h"
#include "AIActorBug.h"
#include "AIActorLizard.h"
#include "Food.h"
#include "TextureLib.h"
#include "Home.h"
#include <algorithm>
#include <iostream>
#include <fstream>

class GameWorld{
private:
	int worldWidth;
	int worldHeight;
	int maxBugs;
	int maxLizards;
	int maxFood;
	int objMap[mapWidth*mapHeight];
	int bugMap[mapWidth*mapHeight];
	int lizMap[mapWidth*mapHeight];
	vector<AIActorLizard*> lizards;
	vector<AIActorBug*> bugs;
	vector<Food*> food;
	vector<Home*> bugHomes;
	vector<Home*> lizHomes;
	vector<Drawable*> walls;
	TextureLib* texLib;
	
public:
	GameWorld(TextureLib* tl = new TextureLib());
	void update(float deltaT);
	void draw();
	void init();
	AIActorLizard* getClosestLizard(AIActorBug* bug);
	AIActorLizard* getClosestLizard(AIActorLizard* liz);
	AIActorBug* getClosestBug(AIActorLizard* liz);
	AIActorBug* getClosestBug(AIActorBug* bug);
	Home* getClosestHome(AIActor* ai, vector<Home*> homes);
	Food* getClosestFood(AIActorBug* bug);
	bool isDead(AIActorBug* bug);
	bool isDead(AIActorLizard* liz);
	vector<AIActorLizard*> getLizards(){return lizards;}
	vector<AIActorBug*> getBugs(){return bugs;}
	int* getBugMap(){return bugMap;}
	int* getLizMap(){return lizMap;}
	void goToNearestBug();
	void goToNearestLizard();
	void getClosestAI(Point2d pos);
	void reset();
	Point2d cameraPos;
	float zoom;
	float screenW;
	int bugCount;
	int lizCount;
	int currentBug;
	int currentLizard;
	bool followMode;
	int bugPopMap[mapWidth*mapHeight];
	int lizPopMap[mapWidth*mapHeight];
	int bugDeathMap[mapWidth*mapHeight];
	int lizDeathMap[mapWidth*mapHeight];
	AIActor* target;
	AIProperties bugStartProperties;
	AIProperties lizStartProperties;
	int minBugs;
	int minLizBugRatio;
	int bugDeaths[3];
	int lizDeaths[2];
};