#include "FSMStateLizardRestAway.h"

FSMStateLizardRestAway::FSMStateLizardRestAway(int state, AIActor* p)
{
	this->type = state;
	this->parent = p;	
}

void FSMStateLizardRestAway::update(float deltaT)
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	liz->setStamina(liz->getStamina() + deltaT*10);
}

void FSMStateLizardRestAway::enter()
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	liz->setSpeed(0);
	liz->atHome = true;
	liz->awayRests++;
}

void FSMStateLizardRestAway::exit()
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	liz->setSpeed(liz->baseSpeed);
	liz->atHome = false;
	liz->updatePercep = true;
}

void FSMStateLizardRestAway::init()
{
}

int FSMStateLizardRestAway::checkTransitions() {
	AIActorLizard* liz = (AIActorLizard*)parent;

	if(liz->getStamina() >= 70)
		return FSM_STATE_LIZARD_WANDER;

	return type;
}