#pragma once
using namespace std;

#include "AIActor.h"
#include "Point2d.h"
#include <random>
#include <math.h>
#include <vector>
#include <time.h>
#include <iostream>

#define sq(x) (x)*(x)

const float PI = 3.14159265359;

const int mapHeight = 24;
const int mapWidth = 30;

const int worldH = 3840;
const int worldW = 4800;

const int cellsize = 160;