#pragma once
#include "glew.h"
#include "Point2d.h"
#include <math.h>
#include <random>
#include <time.h>

class Food{
private:
	Point2d pos;
	float rad;
	GLuint tex;
	

public:
	Food(){}
	Food(GLuint t);
	Food(GLuint t, Point2d pos);
	Point2d getPos(){ return pos; }
	float getRad(){ return rad; }
	void setRad(float r){rad = r;}
	void setPos(int seed, Point2d pos);
	void draw();
	void update(float deltaT);
	float foodVal;
	float ttl;
	float maxSize;
	static int count;
};