#pragma once
using namespace std;
#include "FSMState.h"
#include <vector>

class AIActor;

enum machine {FSM_MACHINE_NONE, FSM_MACHINE_BUG, FSM_MACHINE_LIZARD};

class FSMMachine : public FSMState {
private:
	vector<FSMState*> states;
	FSMState* currentState;
	FSMState* defaultState;
	FSMState* goalState;
	int goalStateID;
	//AIActor* parent;
public:
	FSMMachine(int type = FSM_MACHINE_NONE, AIActor* p = nullptr) { 
		machineType = type; 
		parent = p;
		currentState = new FSMState();
		defaultState = new FSMState();
	}
	virtual void updateMachine(float deltaT);
	virtual void addState(FSMState* state) { states.push_back(state); }
	virtual void setDefaultSate(FSMState* state) { defaultState = state; }
	virtual void setGoalState(FSMState* state) { goalState = state; }
	virtual void setGoalStateID(int stateID) { goalStateID = stateID; }
	virtual bool transitionState(int goalStateID);
	virtual vector<FSMState*>getStates() { return states; }
	virtual void update(float deltaT);
	virtual void enter();
	virtual void exit();
	virtual void init();
	virtual int checkTransitions();

	int getCurrentState(){return currentState->type;}

	int machineType;

	~FSMMachine(){
		while (!states.empty()) {
			FSMState* s = states.back();
			states.pop_back();
			delete s;
		}
	}
};
