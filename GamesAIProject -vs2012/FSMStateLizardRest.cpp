#include "FSMStateLizardRest.h"

FSMStateLizardRest::FSMStateLizardRest(int state, AIActor* p)
{
	this->type = state;
	this->parent = p;	
}

void FSMStateLizardRest::update(float deltaT)
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	liz->setStamina(liz->getStamina() + deltaT*20);
}

void FSMStateLizardRest::enter()
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	liz->setSpeed(0);
	liz->atHome = true;
	liz->awayRests = 0;
}

void FSMStateLizardRest::exit()
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	liz->setSpeed(liz->baseSpeed);
	liz->atHome = false;
	liz->updatePercep = true;
}

void FSMStateLizardRest::init()
{
}

int FSMStateLizardRest::checkTransitions() {
	AIActorLizard* liz = (AIActorLizard*)parent;

	if(liz->getStamina() >= 100)
		return FSM_STATE_LIZARD_WANDER;

	return type;
}