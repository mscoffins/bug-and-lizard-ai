#include "FSMStateLizardWander.h"
#include <iostream>

FSMStateLizardWander::FSMStateLizardWander(int state, AIActor* p)
{
	type = state;
	parent = p;
	resetDelay = 0;
}

void FSMStateLizardWander::update(float deltaT)
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	
	int x,y;
	Point2d pos = liz->getDrawable()->getCentre();
	x = ((int)pos.x + worldW/2)/cellsize;
	y = ((int)pos.y + worldH/2)/cellsize;
	
	if(path.size() == 0 || resetDelay > 3){
		int tx,ty;
		if(liz->nearestBug != NULL){
			if(pos.dist(liz->nearestBug->getDrawable()->getCentre()) > sq(480)){
				tx = ((int)liz->nearestBug->getDrawable()->getCentre().x + worldW/2)/cellsize;
				ty = ((int)liz->nearestBug->getDrawable()->getCentre().y + worldH/2)/cellsize;
			}else{
				tx = x + (rand()%8 - 4);
				ty = y + (rand()%8 - 4);
			}
		}else{
			tx = x + (rand()%8 - 4);
			ty = y + (rand()%8 - 4);
		}
		
		while(tx < 0 || tx > mapWidth-1 || ty < 0 || ty > mapHeight-1 || (ty == y && tx == x) || parent->map[ty * mapWidth + tx] == -1){
			tx = x + (rand()%8 - 4);
			ty = y + (rand()%8 - 4);
		}
		AStar pathfinder(parent->map);
		path = pathfinder.search(Point2d(y,x),Point2d(ty,tx));
		if(path[0].x == y && path[0].y == x){
			path[0].x--;
			path[0].y--;
		}
	}
	
	float px,py;
	px = path.back().y * cellsize - worldW/2 + cellsize/2;
	py = path.back().x * cellsize - worldH/2 + cellsize/2; 
	float angle =  (atan2(py - pos.y,px - pos.x) * 180) / PI;
	float bAngle = liz->getAngle();
	float angleInc = (angle - bAngle);

	liz->setAngle(angle);

	if(sq(pos.x - px) < 64 && sq(pos.y - py) < 64){
		path.pop_back();
	}
	
}

void FSMStateLizardWander::enter()
{
	path =  vector<Point2d>();
	parent->path = &path;
}

void FSMStateLizardWander::exit()
{
	parent->updatePercep = true;
	parent->path = NULL;
}

void FSMStateLizardWander::init()
{
	path =  vector<Point2d>();
}

int FSMStateLizardWander::checkTransitions()
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	Point2d lizPos = parent->getDrawable()->getCentre();

	if(liz->nearestBug != NULL){
		Point2d bugpos = liz->nearestBug->getDrawable()->getCentre();
		int bx = ((int)bugpos.x + worldW/2)/cellsize;
		int by = ((int)bugpos.y + worldH/2)/cellsize;
		float lizr = parent->getDrawable()->getHeight();
		float bugr = liz->nearestBug->getDrawable()->getHeight();
		//cout << liz->getHunger() << endl;
		float db = bugpos.dist(lizPos);
		if(liz->getHunger() >= 50 && db <= sq(160) && liz->map[by * mapWidth + bx] != -1 && !liz->nearestBug->isCaught() && liz->nearestBug->getDrawable()->getWidth() < liz->getDrawable()->getWidth())
			return FSM_STATE_LIZARD_CHASE_BUG;
	}else
		liz->updatePercep = true;

	if(liz->getStamina() < 35)
		return FSM_STATE_LIZARD_GO_HOME;
	if(liz->nearestLizard != NULL){
		AIActorLizard* mate = liz->nearestLizard;
		Point2d matePos = mate->getDrawable()->getCentre();
		float dm = matePos.dist(lizPos);
		if(dm < sq(240) && !liz->nearestLizard->Isbreeding() && mate->getBreedingDelay() == 0 && liz->getBreedingDelay() == 0 && liz->getHunger() < 225 && mate->getHunger() < 225)
			return FSM_STATE_LIZARD_FIND_MATE;
	}else
		liz->updatePercep = true;

	return type;
}