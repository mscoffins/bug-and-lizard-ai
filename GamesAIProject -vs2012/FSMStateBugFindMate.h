#pragma once

#include "FSMState.h"
#include "AIActor.h"
#include "AIActorBug.h"
#include <random>
#include <time.h>

class FSMStateBugFindMate : public FSMState{
private:

public:
	FSMStateBugFindMate(int state, AIActor* p);

	virtual void update(float deltaT);
	virtual void enter();
	virtual void exit();
	virtual void init();
	virtual int checkTransitions();
};