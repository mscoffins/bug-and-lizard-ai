#pragma once
#include "GameWorld.h"
#include "TextureLib.h"
#include "glfont2.h"
#include <string>

enum StatsMode{POP_GRAPHS,HEAT_MAPS_POP,HEAT_MAPS_DEATH,TARGET_STATS,EDIT_PARAMETERS};
class Statistics{
private:
	
	TextureLib* textures;
	GameWorld* world;
	vector<int> bugPopulation;
	vector<int> lizPopulation;
	AIActor* target;
	float updateDelay;
	glfont::GLFont* font;
public:
	Statistics(){}
	Statistics(TextureLib* t, GameWorld* gw, glfont::GLFont* f);
	void init();
	void update(float deltaT);
	void draw();
	void drawBG();
	void drawLizPopGraph();
	void drawBugPopGraph();
	void drawTargetStats();
	void drawBugPopHeatMaps();
	void drawBugDeathHeatMap();
	void drawLizPopHeatMaps();
	void drawLizDeathHeatMap();
	void drawEditParams();
	void editParamInc();
	void editParamDec();
	float* getColourTemp(int val, int min, int max);
	string toString(states s);
	StatsMode mode;
	int selection;
};