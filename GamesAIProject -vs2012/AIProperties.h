#pragma once
#include <iostream>
class AIProperties{
public:
	AIProperties(){}
	AIProperties(float ttl, float sizeMod, float StaminaDecay, float hungerDecay, float baseSpeed, float baseBreedingDelay){
		this->ttl = ttl;
		this->sizeMod = sizeMod;
		this->staminaDecay = StaminaDecay;
		this->hungerDecay = hungerDecay;
		this->baseSpeed = baseSpeed;
		this->baseBreedingDelay = baseBreedingDelay;
	}
	float ttl;
	float sizeMod;
	float staminaDecay;
	float hungerDecay;
	float baseSpeed;
	float baseBreedingDelay;

	AIProperties avg(AIProperties p){
		return AIProperties((this->ttl + p.ttl)/2, (this->sizeMod + p.sizeMod)/2,(this->staminaDecay + p.staminaDecay)/2,(this->hungerDecay + p.hungerDecay)/2,(this->baseSpeed + p.baseSpeed)/2,(this->baseBreedingDelay + p.baseBreedingDelay)/2);
	}

};

inline ostream& operator <<(ostream& o, AIProperties& p){
	return o << p.ttl << "," << p.sizeMod << "," << p.staminaDecay << "," << p.hungerDecay << "," << p.baseSpeed << "," << p.baseBreedingDelay;
}

inline istream& operator >>(istream& i, AIProperties& p){
	char c = ',';
	float t, size,stam,hung,speed,bbd;
	if(i >> t >> c >> size >> c >> stam >> c >> hung >> c >> speed >> c >> bbd)
		p = AIProperties(t, size,stam,hung,speed,bbd);
	else
		i.clear(ios_base::failbit);
	return i;
}