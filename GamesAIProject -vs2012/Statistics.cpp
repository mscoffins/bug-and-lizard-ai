#include "Statistics.h"

Statistics::Statistics(TextureLib* t, GameWorld* gw, glfont::GLFont* f){
	textures = t;
	world = gw;
	bugPopulation = vector<int>(100);
	lizPopulation = vector<int>(100);
	target = nullptr;
	updateDelay = 0;
	font = f;
	mode = StatsMode::POP_GRAPHS;
	selection = 1;
}

void Statistics::init(){
	int bugPop = world->getBugs().size();
	int lizPop = world->getLizards().size();
	for(int i = 0; i < 100; i++){
		bugPopulation[i] = bugPop;
		lizPopulation[i] = lizPop;
	}

}

void Statistics::update(float deltaT){
	if(updateDelay <= 0){
		bugPopulation.push_back(world->getBugs().size());
		bugPopulation.erase(bugPopulation.begin());
		lizPopulation.push_back(world->getLizards().size());
		lizPopulation.erase(lizPopulation.begin());
		updateDelay = 5;
	}else
		updateDelay -= deltaT;

	target = world->target;
}

void Statistics::draw(){
	
	//draw background
	drawBG();
	switch(mode){
		case POP_GRAPHS:
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			font->Begin();
			glColor3f(1.0,1.0,1.0);
			font->DrawString("Population Graphs", 0.3f,1,745);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
			//draw graphs
			drawLizPopGraph();
			drawBugPopGraph();
			break;
		case HEAT_MAPS_POP:
			//draw heatmaps
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			font->Begin();
			glColor3f(1.0,1.0,1.0);
			font->DrawString("Population Heatmaps", 0.3f,1,745);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
			drawBugPopHeatMaps();
			drawLizPopHeatMaps();
			glBegin(GL_QUADS);
				glColor3f(0,0,1);
				glVertex2f(10,10);
				glVertex2f(10,60);
				glColor3f(1,0,0);
				glVertex2f(190,60);
				glVertex2f(190,10);
			glEnd();
			break;
		case HEAT_MAPS_DEATH:
			//draw heatmaps
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			font->Begin();
			glColor3f(1.0,1.0,1.0);
			font->DrawString("Death Heatmaps", 0.3f,1,745);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
			drawBugDeathHeatMap();
			drawLizDeathHeatMap();
			glBegin(GL_QUADS);
				glColor3f(0,0,1);
				glVertex2f(10,10);
				glVertex2f(10,60);
				glColor3f(1,0,0);
				glVertex2f(190,60);
				glVertex2f(190,10);
			glEnd();
			break;
		case TARGET_STATS:
			//draw target Stats
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			font->Begin();
			glColor3f(1.0,1.0,1.0);
			font->DrawString("Target Stats", 0.3f,1,745);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
			drawTargetStats();
			break;
		case EDIT_PARAMETERS:
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			font->Begin();
			glColor3f(1.0,1.0,1.0);
			font->DrawString("Edit Parameters", 0.3f,1,745);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
			drawEditParams();
			break;
	}
	
	

}

void Statistics::drawBG(){
	glPushMatrix();
	//glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, textures->statsBG);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBegin(GL_QUADS);
		glTexCoord2f(0,7.5);glVertex2f(0, 750);
		glTexCoord2f(2,7.5);glVertex2f(200, 750);
		glTexCoord2f(2,0);glVertex2f(200, 0);
		glTexCoord2f(0,0);glVertex2f(0, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	//glDisable(GL_BLEND);
	glPopMatrix();
}

void Statistics::drawBugPopGraph(){

	glColor3f(0.3,0.0,0.3);
	glBegin(GL_QUADS);
		glVertex2f(20, 700);
		glVertex2f(180,700);
		glVertex2f(180,400);
		glVertex2f(20, 400);
	glEnd();
	//axis
	glLineWidth(2);
	glColor3f(0,0,0);
	glBegin(GL_LINE_STRIP);
		glVertex2f(20,700);
		glVertex2f(20,400);
		glVertex2f(180,400);
	glEnd();
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	font->Begin();
	font->DrawString("200",0.15f,1,705);
	font->DrawString("100",0.15f,1,555);
	font->DrawString("0",0.2f,1,405);
	font->DrawString("Time(s)",0.2f,85,395);
	font->DrawString("Bug Population", 0.2f,55,375);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	//line plot
	glLineWidth(1);
	glBegin(GL_LINE_STRIP);
		glColor3f(0.0,1.0,0.0);
		for(int i = 0; i < 100; i++){
			glVertex2f((i*1.5)+25,bugPopulation[i]*1.5 + 400);
		}
	glEnd();
}

void Statistics::drawLizPopGraph(){

	
	glColor3f(0.0,0.45,0.21);
	glBegin(GL_QUADS);
		glVertex2f(20, 350);
		glVertex2f(180,350);
		glVertex2f(180,50);
		glVertex2f(20,50);
	glEnd();
	//axis
	glColor3f(0,0,0);
	glLineWidth(2);
	glBegin(GL_LINE_STRIP);
		glVertex2f(20,350);
		glVertex2f(20,50);
		glVertex2f(180,50);
	glEnd();
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	font->Begin();
	font->DrawString("50",0.2f,1,355);
	font->DrawString("20",0.2f,1,205);
	font->DrawString("0",0.2f,1,55);
	font->DrawString("Time(s)",0.2f,85,45);
	font->DrawString("Lizard Population", 0.2f,55,25);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glLineWidth(1);
	glBegin(GL_LINE_STRIP);
	glColor3f(1.0,0.0,0.0);
	for(int i = 0; i < 100; i++){
		glVertex2f((i*1.5)+25,lizPopulation[i]*6 + 50);
	}
	glEnd();
}

void Statistics::drawTargetStats(){
	if(target != NULL){
		if(target->isAlive()){
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			font->Begin();
			font->DrawString("ID: " + to_string(target->id),0.25f,5,700);
			font->DrawString("Gender: " + target->getGender(),0.25f,5,675);
			font->DrawString("Age: " + to_string((int)target->getAge()),0.25f,5,650);
			font->DrawString("Hunger: " + to_string((int)((target->getHunger()/target->getMaxhunger()) * 100)) + "%",0.25f,5,625);
			font->DrawString("Stamina: " + to_string((int)target->getStamina()) + "%",0.25f,5,600);
			font->DrawString("Breeding delay: " + to_string((int)target->getBreedingDelay()),0.25f,5,575);
			string str;
			switch(target->Isbreeding()){
			case 1:
				str = "True";
			case 0:
				str = "False";
			}
			font->DrawString("Is Breeding: " + str,0.25f,5,550);
			states s = states(target->getBrain()->getCurrentState());
			string state = toString(s);
			font->DrawString("Current State: " + state,0.25f,5,525);
			font->DrawString("Speed: " + to_string(target->getSpeed()),0.25,5,500);
			font->DrawString("Base Trait Modifiers",0.3f,5,450);
			font->DrawString("Hunger decay: " + to_string(target->getHungerDecay()),0.25f,5,425);
			font->DrawString("Stamina decay: " + to_string(target->getStaminaDecay()),0.25f,5,400);
			font->DrawString("Base speed: " + to_string(target->baseSpeed),0.25,5,375);
			font->DrawString("Size modifier: " + to_string(target->getSizeMod()),0.25,5,350);
			font->DrawString("Width: " + to_string(target->getDrawable()->getWidth()),0.25,5,325);
			font->DrawString("Height: " + to_string(target->getDrawable()->getHeight()),0.25,5,300);
			font->DrawString("Max Age: " + to_string(target->getTimeToLive()),0.25f,5,275);
			font->DrawString("Base breeding delay: " + to_string((int)target->baseBreedingDelay),0.25f,5,250);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
		}
		else{
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			font->Begin();
			font->DrawString("No target selected",0.2f,5,700);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_BLEND);
		}

	}else{
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		font->Begin();
		font->DrawString("No target selected",0.2f,5,700);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
	}
}

void Statistics::drawBugPopHeatMaps(){
	int* popMap = world->bugPopMap;
	int count = 0;
	for(int x = 0; x < mapWidth; x++){
		for(int y = 0; y < mapHeight; y++){
			count += popMap[x * mapHeight + y];
			float* col = getColourTemp(popMap[x * mapHeight + y],0,8);
				glColor3f(col[0],col[1],col[2]);
			glBegin(GL_QUADS);
				glVertex2f(180 - 6*x + 6, 450 + 8*y + 8);
				glVertex2f(180 - 6*x, 450 + 8*y + 8);
				glVertex2f(180 - 6*x, 450 + 8*y);
				glVertex2f(180 - 6*x + 6, 450 + 8*y);
			glEnd();
		}
	}
	glColor3f(1,1,1);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	font->Begin();
	string str = "Bug population = " + to_string(count);
	font->DrawString("Bug Population Heatmap",0.25f,5,700);
	font->DrawString(str,0.25f,5,450);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

}

void Statistics::drawBugDeathHeatMap(){
	int* deathMap = world->bugDeathMap;
	int count = 0;
	for(int x = 0; x < mapWidth; x++){
		for(int y = 0; y < mapHeight; y++){
			count += deathMap[x * mapHeight + y];
			float* col = getColourTemp(deathMap[x * mapHeight + y],0,50);
			glColor3f(col[0],col[1],col[2]);
			glBegin(GL_QUADS);
				glVertex2f(180 - 6*x + 6, 500 + 8*y + 8);
				glVertex2f(180 - 6*x, 500 + 8*y + 8);
				glVertex2f(180 - 6*x, 500 + 8*y);
				glVertex2f(180 - 6*x + 6, 500 + 8*y);
			glEnd();
		}
	}
	glColor3f(1,1,1);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	font->Begin();
	string str = "Bug Deaths = " + to_string(count);
	font->DrawString("Bug Deaths Heatmap",0.25f,5,715);
	font->DrawString(str,0.25f,5,500);
	font->DrawString("Old Age Deaths: " + to_string(world->bugDeaths[0]),0.25f,5,475);
	font->DrawString("Hunger Deaths: " + to_string(world->bugDeaths[1]),0.25f,5,450);
	font->DrawString("Eaten Deaths: " + to_string(world->bugDeaths[2]),0.25f,5,425);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

void Statistics::drawLizPopHeatMaps(){
	int* popMap = world->lizPopMap;
	int count = 0;
	for(int x = 0; x < mapWidth; x++){
		for(int y = 0; y < mapHeight; y++){
			count += popMap[x * mapHeight +y];
			float* col = getColourTemp(popMap[x * mapHeight + y],0,4);
			glColor3f(col[0],col[1],col[2]);
			glBegin(GL_QUADS);
				glVertex2f(180 - 6*x + 6, 150 + 8*y + 8);
				glVertex2f(180 - 6*x, 150 + 8*y + 8);
				glVertex2f(180 - 6*x, 150 + 8*y);
				glVertex2f(180 - 6*x + 6, 150 + 8*y);
			glEnd();
		}
	}
	glColor3f(1,1,1);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	font->Begin();
	string str = "Lizard population = " + to_string(count);
	font->DrawString("Lizard Population Heatmap",0.25f,5,400);
	font->DrawString(str,0.25f,5,150);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

void Statistics::drawLizDeathHeatMap(){
	int* deathMap = world->lizDeathMap;
	int count = 0;
	for(int x = 0; x < mapWidth; x++){
		for(int y = 0; y < mapHeight; y++){
			count += deathMap[x * mapHeight + y];
			float* col = getColourTemp(deathMap[x * mapHeight + y],0,10);
			glColor3f(col[0],col[1],col[2]);
			glBegin(GL_QUADS);
				glVertex2f(180 - 6*x + 6, 150 + 8*y + 8);
				glVertex2f(180 - 6*x, 150 + 8*y + 8);
				glVertex2f(180 - 6*x, 150 + 8*y);
				glVertex2f(180 - 6*x + 6, 150 + 8*y);
			glEnd();
		}
	}
	glColor3f(1,1,1);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	font->Begin();
	string str = "Lizard Deaths = " + to_string(count);
	font->DrawString("Lizard Deaths Heatmap",0.25f,5,375);
	font->DrawString(str,0.25f,5,150);
	font->DrawString("Old Age Deaths: " + to_string(world->lizDeaths[0]),0.25f,5,125);
	font->DrawString("Hunger Deaths: " + to_string(world->lizDeaths[1]),0.25f,5,100);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

void Statistics::drawEditParams(){
	glColor3f(0.2,0.4,0.8);
	glBegin(GL_QUADS);
	if(selection <= 6){
		glVertex2f(5,675 - 25*(selection-1));
		glVertex2f(5,675 - 25*selection);
		glVertex2f(195,675 - 25*selection);
		glVertex2f(195,675 - 25*(selection-1));	
	}
	else{
		glVertex2f(5,450 - 25*(selection-6));
		glVertex2f(5,450 - 25*(selection-7));
		glVertex2f(195,450 - 25*(selection-7));
		glVertex2f(195,450 - 25*(selection-6));
	}
	glEnd();
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	font->Begin();
	font->DrawString("Bug Parameters",0.3,5,700);
	font->DrawString("Time To Live: " + to_string(world->bugStartProperties.ttl),0.25,5,675);
	font->DrawString("Size Modifier: " + to_string(world->bugStartProperties.sizeMod),0.25,5,650);
	font->DrawString("Stamina Decay: " + to_string(world->bugStartProperties.staminaDecay),0.25,5,625);
	font->DrawString("Hunger Decay: " + to_string(world->bugStartProperties.hungerDecay),0.25,5,600);
	font->DrawString("Base Speed: " + to_string(world->bugStartProperties.baseSpeed),0.25,5,575);
	font->DrawString("Mating Delay: " + to_string(world->bugStartProperties.baseBreedingDelay),0.25,5,550);

	font->DrawString("Lizard Parameters",0.3,5,475);
	font->DrawString("Time To Live: " + to_string(world->lizStartProperties.ttl),0.25,5,450);
	font->DrawString("Size Modifier: " + to_string(world->lizStartProperties.sizeMod),0.25,5,425);
	font->DrawString("Stamina Decay: " + to_string(world->lizStartProperties.staminaDecay),0.25,5,400);
	font->DrawString("Hunger Decay: " + to_string(world->lizStartProperties.hungerDecay),0.25,5,375);
	font->DrawString("Base Speed: " + to_string(world->lizStartProperties.baseSpeed),0.25,5,350);
	font->DrawString("Mating Delay: " + to_string(world->lizStartProperties.baseBreedingDelay),0.25,5,325);

	font->DrawString("Press Space to reset",0.25,5,250);
	font->DrawString("world and apply changes",0.25,5,225);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}

float* Statistics::getColourTemp(int val, int min, int max){
	int mid = (max - min )/2;
	float rgb[3];
	rgb[1] = 0;

	if(val >= mid){
		rgb[0] = 1;
		rgb[2] = ((float)max - (float)val)/((float)max - (float)mid);
	}else{
		rgb[2] = 1;
		rgb[0] = ((float)val - (float)min)/((float)mid - (float)min);
	}

	return rgb;
}

string Statistics::toString(states s){
	switch(s){
		case FSM_STATE_NONE:
			return "None";
		case FSM_STATE_BUG_WANDER:
			return "Wander";
		case FSM_STATE_BUG_EAT_FOOD:
			return "Eat Food";
		case FSM_STATE_BUG_RUN_AWAY:
			return "Run Away";
		case FSM_STATE_BUG_GO_HOME: 
			return "Go Home";
		case FSM_STATE_BUG_REST: 
			return "Rest";
		case FSM_STATE_BUG_FIND_MATE: 
			return "Find Mate";
		case FSM_STATE_BUG_MATE:
			return "Mating";
		case FSM_STATE_LIZARD_WANDER: 
			return "Wander";
		case FSM_STATE_LIZARD_CHASE_BUG: 
			return "Chase Bug";
		case FSM_STATE_LIZARD_EAT_BUG: 
			return "Eat Bug";
		case FSM_STATE_LIZARD_GO_HOME: 
			return "Go home";
		case FSM_STATE_LIZARD_REST: 
			return "Rest";
		case FSM_STATE_LIZARD_FIND_MATE: 
			return "Find Mate";
		case FSM_STATE_LIZARD_MATE: 
			return "Mating";
		case FSM_STATE_LIZARD_REST_AWAY:
			return "Rest Away";
	}

}

void Statistics::editParamInc(){
	switch(selection){
		case 1:
			if(world->bugStartProperties.ttl < 300)
				world->bugStartProperties.ttl++;
			break;
		case 2:
			if(world->bugStartProperties.sizeMod < 2)
				world->bugStartProperties.sizeMod += 0.01;
			break;
		case 3:
			if(world->bugStartProperties.staminaDecay < 10)
				world->bugStartProperties.staminaDecay += 0.05;
			break;
		case 4:
			if(world->bugStartProperties.hungerDecay < 10)
				world->bugStartProperties.hungerDecay += 0.05;
			break;
		case 5:
			if(world->bugStartProperties.baseSpeed < 250)
				world->bugStartProperties.baseSpeed++;
			break;
		case 6:
			if(world->bugStartProperties.baseBreedingDelay < 40)
				world->bugStartProperties.baseBreedingDelay += 0.5;
			break;
		case 7:
			if(world->lizStartProperties.ttl < 400)
				world->lizStartProperties.ttl++;
			break;
		case 8:
			if(world->lizStartProperties.sizeMod < 2)
				world->lizStartProperties.sizeMod += 0.01;
			break;
		case 9:
			if(world->lizStartProperties.staminaDecay < 10)
				world->lizStartProperties.staminaDecay += 0.05;
			break;
		case 10:
			if(world->lizStartProperties.hungerDecay < 20)
				world->lizStartProperties.hungerDecay += 0.05;
			break;
		case 11:
			if(world->lizStartProperties.baseSpeed < 250)
				world->lizStartProperties.baseSpeed++;
			break;
		case 12:
			if(world->lizStartProperties.baseBreedingDelay < 40)
				world->lizStartProperties.baseBreedingDelay += 0.5;
			break;
	}
}
void Statistics::editParamDec(){
	switch(selection){
		case 1:
			if(world->bugStartProperties.ttl > 30)
				world->bugStartProperties.ttl--;
			break;
		case 2:
			if(world->bugStartProperties.sizeMod > 0.5)
				world->bugStartProperties.sizeMod -= 0.01;
			break;
		case 3:
			if(world->bugStartProperties.staminaDecay > 0.5)
				world->bugStartProperties.staminaDecay -= 0.05;
			break;
		case 4:
			if(world->bugStartProperties.hungerDecay > 1)
				world->bugStartProperties.hungerDecay -= 0.05;
			break;
		case 5:
			if(world->bugStartProperties.baseSpeed > 50)
				world->bugStartProperties.baseSpeed--;
			break;
		case 6:
			if(world->bugStartProperties.baseBreedingDelay > 5)
				world->bugStartProperties.baseBreedingDelay -= 0.5;
			break;
		case 7:
			if(world->lizStartProperties.ttl > 50)
				world->lizStartProperties.ttl--;
			break;
		case 8:
			if(world->lizStartProperties.sizeMod > 0.5)
				world->lizStartProperties.sizeMod -= 0.01;
			break;
		case 9:
			if(world->lizStartProperties.staminaDecay > 0.5)
				world->lizStartProperties.staminaDecay -= 0.05;
			break;
		case 10:
			if(world->lizStartProperties.hungerDecay > 1)
				world->lizStartProperties.hungerDecay -= 0.05;
			break;
		case 11:
			if(world->lizStartProperties.baseSpeed > 50)
				world->lizStartProperties.baseSpeed--;
			break;
		case 12:
			if(world->lizStartProperties.baseBreedingDelay > 5)
				world->lizStartProperties.baseBreedingDelay -= 0.5;
			break;
	}
}

