#pragma once
#include "FSMState.h"
#include "AIActor.h"
#include "AIActorBug.h"
#include <random>
#include <time.h>

class FSMStateBugRunAway : public FSMState{
private:
	float resetDelay;
	vector<Point2d> path;
public:
	FSMStateBugRunAway(int s, AIActor* p);
	virtual void update(float deltaT);
	virtual void enter();
	virtual void exit();
	virtual void init();
	virtual int checkTransitions();
};