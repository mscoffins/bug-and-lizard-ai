#include "FSMMachine.h"

void FSMMachine::updateMachine(float deltaT)
{
	//no states available to the machine
	if (states.size() == 0)
		return;
	//if no state is set, return to the default state
	if (currentState->type == 0)
		currentState = defaultState;
	//if still not state set, not states are available so exit
	if (currentState->type == 0)
		return;
	//store current state, check state transition
	int oldState = currentState->type;
	goalStateID = currentState->checkTransitions();
	//if transitioning state differs from current state, make switch
	if (goalStateID != currentState->type) {
		//if state thats being switch to is in the state list, 
		//exit current state and enter new state
		if (transitionState(goalStateID)) {
			currentState->exit();
			currentState = goalState;
			currentState->enter();
		}
	}
	//execute current states update method
	currentState->update(deltaT);
}

bool FSMMachine::transitionState(int goalStateID)
{
	for each (FSMState* state in states){
		if (state->type == goalStateID) {
			goalState = state;
			return true;
		}
	}
	return false;
}

void FSMMachine::update(float deltaT)
{
}

void FSMMachine::enter()
{
}

void FSMMachine::exit()
{
}

void FSMMachine::init()
{
}

int FSMMachine::checkTransitions()
{
	return 0;
}
