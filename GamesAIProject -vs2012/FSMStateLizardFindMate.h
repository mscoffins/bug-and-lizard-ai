#pragma once

#include "FSMState.h"
#include "AIActor.h"
#include "AIActorLizard.h"
#include <random>
#include <time.h>

class FSMStateLizardFindMate : public FSMState{
private:

public:
	FSMStateLizardFindMate(int state, AIActor* p);

	virtual void update(float deltaT);
	virtual void enter();
	virtual void exit();
	virtual void init();
	virtual int checkTransitions();
};