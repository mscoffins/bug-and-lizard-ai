#include "AIActorBug.h"
#include <iostream>

int AIActorBug::CurrentID = 0;

AIActorBug::AIActorBug(Drawable* bug, Gender s, int* m)
{
	//unique identifier and rand seed
	id = CurrentID++;
	srand(time(NULL) + id);
	
	//drawable object
	actor = bug;
	//gender
	sex = s;

	/************************
	****	  Flags      ****
	*************************/
	alive = true;
	caught = false;
	atHome = false;
	breeding = false;
	childFlag = false;
	updatePercep = true;
	/************************
	**** default values  ****
	*************************/
	caughtDelay = 0;
	breedDelayReset = 0;
	hunger = 0;
	stamina = 100;
	breedingDelay = 15;
	age = 0;
	//pathfinder = AStar(m);
	map = m;
	percepUpdDelay = 0;
	nearestHome = NULL;
	maxHunger = 130;
	path = NULL;
	/************************
	****	Modifiers    ****
	*************************/
	//starting angle/direction
	float temp;
	default_random_engine generator(time(NULL) + id);
	normal_distribution<float> speedGen(100,3);
	normal_distribution<float> ttlGen(110,5);
	normal_distribution<float> hungerGen(1,0.1);
	normal_distribution<float> staminaGen(3,0.3);
	normal_distribution<float> sizeGen(1,0.07);
	angle = rand()%360;
	//speed modifier, 110 +/- 10
	//temp = (100 + (rand()%20 - 10));
	//baseSpeed = temp;
	baseSpeed = (int)speedGen(generator);
	speed = baseSpeed;
	//time to live modifier, 110 +/- 15 seconds
	//temp = (110 + (rand()%30-15));
	//ttl = temp;
	ttl = (int)ttlGen(generator);
	//rate of hunger decay, 7 +/- 1 per second
	//temp = 6 + rand()%20;
	//hungerDecay = temp/10;
	hungerDecay = hungerGen(generator);
	//rate of stamina decay, 3 +/- 1 per second
	//temp = 20 + rand()%20;
	//staminaDecay = temp/10;
	staminaDecay = staminaGen(generator);
	//max size modifier, 25x25 +/- 20%
	//temp = (100 + rand()%40 - 20);
	sizeMod = sizeGen(generator);
	
	maxHeight = 25 * sizeMod;
	maxWidth = 25 * sizeMod;
	//spaw at 1/7 of max size
	actor->setWidth(maxWidth/7);
	actor->setHeight(maxHeight/7);

	/**********************
	 ***** Debugging  *****
	 **********************/

	causeOfDeath = "";

}
//constructor to pass on genetic alterations to traits
AIActorBug::AIActorBug(Drawable* bug, Gender s, int* m, AIProperties p)
{
	//unique identifier and rand seed
	id = CurrentID++;
	srand(time(NULL) + id);
	
	//drawable object
	actor = bug;
	//gender
	sex = s;

	/************************
	****	  Flags      ****
	*************************/
	alive = true;
	caught = false;
	atHome = false;
	breeding = false;
	childFlag = false;
	updatePercep = true;
	/************************
	**** default values  ****
	*************************/
	caughtDelay = 0;
	breedDelayReset = 0;
	hunger = 0;
	stamina = 100;
	breedingDelay = 15;
	age = 0;
	//pathfinder = AStar(m);
	map = m;
	percepUpdDelay = 0;
	nearestHome = NULL;
	maxHunger = 130;
	path = NULL;
	/************************
	****	Modifiers    ****
	*************************/
	float temp;
	default_random_engine generator(time(NULL) + id);
	normal_distribution<float> speedGen(p.baseSpeed,3);
	normal_distribution<float> ttlGen(p.ttl,5);
	normal_distribution<float> hungerGen(p.hungerDecay,0.1);
	normal_distribution<float> staminaGen(p.staminaDecay,0.3);
	normal_distribution<float> sizeGen(p.sizeMod,0.07);
	normal_distribution<float> breedDelayGen(p.baseBreedingDelay,1);
	//set random spawn angle/direction
	angle = rand()%360;
	//speed modifier, 110 +/- 10
	//temp = (130 + (rand()%20 - 10));
	temp = (int)speedGen(generator);
	baseSpeed = temp;
	speed = temp;
	//time to live modifier, 190 +/- 20 seconds
	//temp = (190 + (rand()%40 - 20));
	temp = (int)ttlGen(generator);
	ttl = temp;
	//rate of hunger decay, 8 +/- 2 per second
	//temp = 60 + rand()%40;
	//hungerDecay = temp/10;
	hungerDecay = hungerGen(generator);
	if(hungerDecay < 1)
		hungerDecay = 1;
	//rate of stamina decay, 2 - 1 per second
	//temp = rand()%20;
	//staminaDecay = 1 + temp/10;
	staminaDecay = staminaGen(generator);
	if(staminaDecay < 1.5)
		staminaDecay = 1.5;

	baseBreedingDelay = breedDelayGen(generator);
	breedingDelay = baseBreedingDelay;
	//max size modifier, 100x50 +/- 20%
	//temp = (100 + rand()%40 - 20);
	sizeMod = sizeGen(generator);
	
	maxHeight = 25 * sizeMod;
	maxWidth = 25 * sizeMod;
	//spaw at 1/7 of max size
	actor->setWidth(maxWidth/7);
	actor->setHeight(maxHeight/7);

	/**********************
	 ***** Debugging  *****
	 **********************/

	causeOfDeath = "";

}

//draw bug at specified angle
void AIActorBug::draw()
{
		actor->draw(angle);
}

void AIActorBug::update(float deltaT)
{
	/************************
	****   Flag checks   ****
	*************************/
	//die of old age
	if(age > ttl){
		alive = false;
		causeOfDeath = "Old Age";
	}
	//die of hunger
	if(hunger >= maxHunger){
		alive = false;
		causeOfDeath = "Hunger";
	}
	//adjust speed relative to stamina
	if(stamina < 50){
		speed = baseSpeed*(stamina/100);
		if(speed < 35)
			speed = 35;
	}
	if(stamina > 100)
		stamina = 100;
	//handle caught flag (being eaten)
	if(caught){
		speed = 0;
		caughtDelay += deltaT;
	}
	//handle case where bug gets caught but isnt eaten and isnt freed
	if(caughtDelay > 5){
		caught = false;
		caughtDelay = 0;
	}
	//stop moving if breeding
	if(breeding){
		speed = 0;
		breedDelayReset += deltaT;
	}
	if(breedDelayReset > 5){
		breeding = false;
		breedDelayReset = 0;
	}
	//stop moving if at home resting
	if(atHome)
		speed = 0;

	/************************
	****Update parameters****
	*************************/
	//decrement breeding delay, cap at 0
	breedingDelay -=deltaT;
	if(breedingDelay < 0)
		breedingDelay = 0;
	

	//grow bug if not already at max size
	if(actor->getHeight() < maxHeight)
		actor->setHeight(actor->getHeight() + deltaT);
	if(actor->getWidth() < maxWidth)
		actor->setWidth(actor->getWidth() + deltaT);
	//age bug
	age += deltaT;
	//add frame hunger
	hunger += hungerDecay*deltaT;
	//decay stamina vased on speed
	stamina -= staminaDecay * deltaT * speed/baseSpeed;
	//update perception delay
	if(!updatePercep)
		percepUpdDelay += deltaT;
	else
		percepUpdDelay = 0;
	if(percepUpdDelay > 3){
		updatePercep = true;
		percepUpdDelay = 0;
	}

	//update texture
	actor->update(deltaT * 5 * (speed/10));
	//update and execute frame of current brain state
	brain->updateMachine(deltaT);

	//border collisions
	if(actor->getCentre().x < - worldW/2)
		angle = 0;
	if(actor->getCentre().x > worldW/2)
		angle = 180;
	if(actor->getCentre().y < - worldH/2) 
		angle = 90;
	if(actor->getCentre().y > worldH/2)
		angle = -90;
	if(angle > 180)
		angle -= 360;
	if(angle < -180)
		angle += 360;
	//position update
	float x = actor->getCentre().x + speed*deltaT*cos(angle*3.141f / 180.0f);
	float y = actor->getCentre().y + speed*deltaT*sin(angle*3.141f / 180.0f);
	actor->setCentre(Point2d(x, y));

}

void AIActorBug::init()
{
	//create brain and add states
	brain = new FSMMachine(FSM_MACHINE_BUG, this);
	brain->addState(new FSMStateBugWander(FSM_STATE_BUG_WANDER,this));
	brain->addState(new FSMStateBugEatFood(FSM_STATE_BUG_EAT_FOOD,this));
	brain->addState(new FSMStateBugRunAway(FSM_STATE_BUG_RUN_AWAY,this));
	brain->addState(new FSMStateBugGoHome(FSM_STATE_BUG_GO_HOME,this));
	brain->addState(new FSMStateBugRest(FSM_STATE_BUG_REST,this));
	//only let females initiate mating
	if(sex == FEMALE){
		brain->addState(new FSMStateBugFindMate(FSM_STATE_BUG_FIND_MATE,this));
		brain->addState(new FSMStateBugMate(FSM_STATE_BUG_MATE,this));
	}
	//set default state to wander
	brain->setDefaultSate(brain->getStates().at(0));
}

void AIActorBug::updatePerceptions(AIActorLizard* liz,AIActorBug* b,Food* f, Home* nearHome){
	nearestLiz = liz;
	nearestFood = f;
	nearestBug = b;
	nearestHome = nearHome;
}

void AIActorBug::spawnAtHome(){
	actor->setCentre(nearestHome->getDrawable()->getCentre());
}