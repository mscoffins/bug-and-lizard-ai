#include "FSMStateBugRest.h"

FSMStateBugRest::FSMStateBugRest(int state, AIActor* p)
{
	this->type = state;
	this->parent = p;	
}

void FSMStateBugRest::update(float deltaT)
{
	AIActorBug* bug = (AIActorBug*)parent;
	bug->setStamina(bug->getStamina() + deltaT*20);
}

void FSMStateBugRest::enter()
{
	AIActorBug* bug = (AIActorBug*)parent;
	bug->setSpeed(0);
	bug->atHome = true;
}

void FSMStateBugRest::exit()
{
	AIActorBug* bug = (AIActorBug*)parent;
	bug->setSpeed(bug->baseSpeed);
	bug->atHome = false;
	bug->updatePercep = true;
}

void FSMStateBugRest::init()
{
}

int FSMStateBugRest::checkTransitions() {
	AIActorBug* bug = (AIActorBug*)parent;

	if(bug->getStamina() >= 100)
		return FSM_STATE_BUG_WANDER;

	return type;
}