#pragma once
#include "FSMState.h"
#include "FSMMachine.h"
//#include "AStar.h"
#include "Drawable.h"
#include "Home.h"
#include "AIProperties.h"

enum Gender{MALE,FEMALE};

class AIActor {
public:
	AIActor();
	virtual void draw();
	virtual void update(float deltaT);
	virtual void init();
	Drawable* getDrawable() { return actor; }
	float getAngle() { return angle; }
	float getSpeed() { return speed; }
	FSMMachine* getBrain() { return brain; }
	void setAngle(float a) { angle = a; }
	void setSpeed(float s) { speed = s; }
	float getHunger(){return hunger;}
	void setHunger(float h){hunger = h;}
	float getStamina(){return stamina;}
	void setStamina(float s){stamina = s;}
	float getBreedingDelay(){return breedingDelay;}
	void setBreedingDelay(float bd){breedingDelay = bd;}
	float getHungerDecay(){return hungerDecay;}
	float getStaminaDecay(){return staminaDecay;}
	float getTimeToLive(){return ttl;}
	float getSizeMod(){return sizeMod;}
	bool Isbreeding(){return breeding;}
	void setBreeding(bool b){breeding = b;}
	float getAge(){return age;}
	bool isAlive(){return alive;}
	float getMaxhunger(){return maxHunger;}
	string getGender(){return (sex == MALE)? "Male": "Female";}
	AIProperties getPropeties(){return AIProperties(ttl,sizeMod,staminaDecay,hungerDecay,baseSpeed,baseBreedingDelay);}
	int id;
	Home* nearestHome;
	bool atHome;
	bool childFlag;
	Gender sex;
	float baseSpeed;
	float baseBreedingDelay;
	char* causeOfDeath;
	int* map;
	bool updatePercep;
	vector<Point2d>* path;
protected:
	
	Drawable* actor;
	bool alive;
	float angle;
	float speed;
	float hunger;
	float stamina;
	float breedingDelay;
	float age;
	float ttl;
	float sizeMod;
	float maxWidth;
	float maxHeight;
	bool breeding;
	float breedingRate;
	float staminaDecay;
	float hungerDecay;
	FSMMachine* brain;
	float percepUpdDelay;
	float maxHunger;
};