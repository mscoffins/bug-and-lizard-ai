#include "AIActorLizard.h"

int AIActorLizard::CurrentID = 0;

AIActorLizard::AIActorLizard(Drawable* a, Gender s, int* m){
	//unique identifier and rand seed
	id = CurrentID++;
	srand(time(NULL) + id);
	
	//drawable object
	actor = a;
	//gender
	sex = s;

	/************************
	****	  Flags      ****
	*************************/
	alive = true;
	atHome = false;
	breeding = false;
	childFlag = false;
	updatePercep = true;
	/************************
	**** default values  ****
	*************************/
	age = 0;
	hunger = 0;
	breedingDelay = 20;
	stamina = 100;
	awayRests = 0;
	//pathfinder = AStar(m);
	map = m;
	percepUpdDelay = 0;
	nearestHome = NULL;
	maxHunger = 400;
	path = NULL;
	/************************
	****	Modifiers    ****
	*************************/
	float temp;
	default_random_engine generator(time(NULL) + id);
	normal_distribution<float> speedGen(130,3);
	normal_distribution<float> ttlGen(190,7);
	normal_distribution<float> hungerGen(8,0.7);
	normal_distribution<float> staminaGen(2,0.3);
	normal_distribution<float> sizeGen(1,0.07);
	//set random spawn angle/direction
	angle = rand()%360;
	//speed modifier, 110 +/- 10
	//temp = (130 + (rand()%20 - 10));
	temp = (int)speedGen(generator);
	baseSpeed = temp;
	speed = temp;
	//time to live modifier, 190 +/- 20 seconds
	//temp = (190 + (rand()%40 - 20));
	temp = (int)ttlGen(generator);
	ttl = temp;
	//rate of hunger decay, 8 +/- 2 per second
	//temp = 60 + rand()%40;
	//hungerDecay = temp/10;
	hungerDecay = hungerGen(generator);
	//rate of stamina decay, 2 - 1 per second
	//temp = rand()%20;
	//staminaDecay = 1 + temp/10;
	staminaDecay = staminaGen(generator);
	//max size modifier, 100x50 +/- 20%
	//temp = (100 + rand()%40 - 20);
	sizeMod = sizeGen(generator);
	maxHeight = 100 * sizeMod;
	maxWidth = 50 * sizeMod;
	//spawn at 1/5 of max size
	actor->setWidth(maxWidth/5);
	actor->setHeight(maxHeight/5);

	/**********************
	 ***** Debugging  *****
	 **********************/

	causeOfDeath = "";
	
}
//constructor to pass on genetic traits
AIActorLizard::AIActorLizard(Drawable* a, Gender s, int* m, AIProperties p){
	//unique identifier and rand seed
	id = CurrentID++;
	srand(time(NULL) + id);
	
	//drawable object
	actor = a;
	//gender
	sex = s;

	/************************
	****	  Flags      ****
	*************************/
	alive = true;
	atHome = false;
	breeding = false;
	childFlag = false;
	updatePercep = true;
	/************************
	**** default values  ****
	*************************/
	age = 0;
	hunger = 0;
	breedingDelay = 25;
	stamina = 100;
	awayRests = 0;
	//pathfinder = AStar(m);
	map = m;
	percepUpdDelay = 0;
	nearestHome = NULL;
	maxHunger = 400;
	path = NULL;
	/************************
	****	Modifiers    ****
	*************************/
	float temp;
	default_random_engine generator(time(NULL) + id);
	normal_distribution<float> speedGen(p.baseSpeed,3);
	normal_distribution<float> ttlGen(p.ttl,7);
	normal_distribution<float> hungerGen(p.hungerDecay,0.7);
	normal_distribution<float> staminaGen(p.staminaDecay,0.3);
	normal_distribution<float> sizeGen(p.sizeMod,0.07);
	normal_distribution<float> breedDelayGen(p.baseBreedingDelay,1);
	//set random spawn angle/direction
	angle = rand()%360;
	//speed modifier, 110 +/- 10
	//temp = (130 + (rand()%20 - 10));
	temp = (int)speedGen(generator);
	baseSpeed = temp;
	speed = temp;
	//time to live modifier, 190 +/- 20 seconds
	//temp = (190 + (rand()%40 - 20));
	temp = (int)ttlGen(generator);
	ttl = temp;
	//rate of hunger decay, 8 +/- 2 per second
	//temp = 60 + rand()%40;
	//hungerDecay = temp/10;
	hungerDecay = hungerGen(generator);
	if(hungerDecay < 4)
		hungerDecay = 4;
	//rate of stamina decay, 2 - 1 per second
	//temp = rand()%20;
	//staminaDecay = 1 + temp/10;
	staminaDecay = staminaGen(generator);
	if(staminaDecay < 0.5)
		staminaDecay = 0.5;

	baseBreedingDelay = breedDelayGen(generator);
	breedingDelay = baseBreedingDelay;
	//max size modifier, 100x50 +/- 20%
	//temp = (100 + rand()%40 - 20);
	sizeMod = sizeGen(generator);
	
	maxHeight = 100 * sizeMod;
	maxWidth = 50 * sizeMod;
	//spawn at 1/5 of max size
	actor->setWidth(maxWidth/5);
	actor->setHeight(maxHeight/5);

	/**********************
	 ***** Debugging  *****
	 **********************/

	causeOfDeath = "";
	
}

void AIActorLizard::draw(){
	actor->draw(angle);
}

void AIActorLizard::init(){
	//create brain
	brain = new FSMMachine(FSM_MACHINE_LIZARD,this);
	brain->addState(new FSMStateLizardWander(FSM_STATE_LIZARD_WANDER,this));
	brain->addState(new FSMStateLizardChaseBug(FSM_STATE_LIZARD_CHASE_BUG,this));
	brain->addState(new FSMStateLizardEatBug(FSM_STATE_LIZARD_EAT_BUG,this));
	brain->addState(new FSMStateLizardGoHome(FSM_STATE_LIZARD_GO_HOME,this));
	brain->addState(new FSMStateLizardRest(FSM_STATE_LIZARD_REST,this));
	brain->addState(new FSMStateLizardRestAway(FSM_STATE_LIZARD_REST_AWAY,this));
	//only let females initiate mating
	if(sex == FEMALE){
		brain->addState(new FSMStateLizardFindMate(FSM_STATE_LIZARD_FIND_MATE,this));
		brain->addState(new FSMStateLizardMate(FSM_STATE_LIZARD_MATE,this));
	}
	//set default state to wander
	brain->setDefaultSate(brain->getStates().at(0));
}

void AIActorLizard::update(float deltaT){
	//die of old age
	if(age > ttl){
		alive = false;
		causeOfDeath  = "Old Age";
	}
	//die of hunger
	if(hunger >= maxHunger){
		alive = false;
		causeOfDeath = "Hunger";
	}
	//adjust speed relative to stamina
	if(stamina < baseSpeed/2){
		if( brain->getCurrentState() != FSM_STATE_LIZARD_EAT_BUG ){
			speed = baseSpeed*(stamina/100);
			if(speed < 30)
				speed = 30;
		}
		
	}
	if(stamina > 100)
		stamina = 100;
	//dont move if breeding
	if(breeding)
		speed = 0;
	//dont move is resting at home
	if(atHome)
		speed = 0;
	
	//decrement breeding delay time, cap at 0
	breedingDelay -= deltaT;
	if(breedingDelay < 0)
		breedingDelay = 0;

	//grow lizard if not at max size
	if(actor->getHeight() < maxHeight)
		actor->setHeight(actor->getHeight() + 6*deltaT);
	if(actor->getWidth() < maxWidth)
		actor->setWidth(actor->getWidth() + 3*deltaT);

	//decay hunger and stamina
	if(hunger < 0)
		hunger = 0;
	hunger += hungerDecay*deltaT;
	stamina -= staminaDecay*deltaT * speed/baseSpeed;

	//age lizard
	age += deltaT;

	//update perception delay
	if(!updatePercep)
		percepUpdDelay += deltaT;
	else
		percepUpdDelay = 0;
	if(percepUpdDelay > 3){
		updatePercep = true;
		percepUpdDelay = 0;
	}

	//update texture
	actor->update(deltaT * 7.5 * (speed/80));
	//update and execute current brain state
	brain->updateMachine(deltaT);

	//boundry collisions
	if(actor->getCentre().x < - worldW/2)
		angle = 0;
	if(actor->getCentre().x > worldW/2)
		angle = 180;
	if(actor->getCentre().y < - worldH/2) 
		angle = 90;
	if(actor->getCentre().y > worldH/2)
		angle = -90;

	if(!breeding && !atHome){
	//update position
		float x = actor->getCentre().x + speed*deltaT*cos(angle*3.141f / 180.0f);
		float y = actor->getCentre().y + speed*deltaT*sin(angle*3.141f / 180.0f);
		actor->setCentre(Point2d(x, y));
	}
}

void AIActorLizard::updatePerceptions(AIActorBug* bug, AIActorLizard* l, Home* nearHome){
	nearestBug = bug;
	nearestLizard = l;
	nearestHome = nearHome;
}

void AIActorLizard::spawnAtHome(){
	actor->setCentre(nearestHome->getDrawable()->getCentre());
}