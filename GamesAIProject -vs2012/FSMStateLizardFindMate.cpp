#include "FSMStateLizardFindMate.h"

FSMStateLizardFindMate::FSMStateLizardFindMate(int s, AIActor* p){
	type = s;
	parent = p;
}
void FSMStateLizardFindMate::update(float deltaT){

	AIActorLizard* liz = (AIActorLizard*)parent;
	Point2d lizpos = parent->getDrawable()->getCentre();
	if(liz->nearestLizard != NULL){
		Point2d matepos = liz->nearestLizard->getDrawable()->getCentre();
		parent->setAngle(180/3.141f * atan2f(matepos.y - lizpos.y,matepos.x - lizpos.x));
	}
	
}
void FSMStateLizardFindMate::enter(){
	AIActorLizard* liz = (AIActorLizard*)parent;
	Point2d lizpos = parent->getDrawable()->getCentre();
	if(liz->nearestLizard != NULL){
		Point2d matepos = liz->nearestLizard->getDrawable()->getCentre();
		parent->setAngle(180/3.141f * atan2f(matepos.y - lizpos.y,matepos.x - lizpos.x));
		parent->setSpeed(parent->baseSpeed + 40);
	}
}
void FSMStateLizardFindMate::exit(){
	parent->setSpeed(parent->baseSpeed);
	parent->updatePercep = true;
}
void FSMStateLizardFindMate::init(){

}

int FSMStateLizardFindMate::checkTransitions(){
	
	AIActorLizard* liz = (AIActorLizard*)parent;
	Point2d lizPos = parent->getDrawable()->getCentre();

	if(liz->nearestBug != NULL){
		Point2d bugpos = liz->nearestBug->getDrawable()->getCentre();
		float lizr = parent->getDrawable()->getHeight();
		//cout << liz->getHunger() << endl;
		float db = bugpos.dist(lizPos);
		if(liz->getHunger() >= 250 && db <= sq(160))
			return FSM_STATE_LIZARD_CHASE_BUG;
	}else
		liz->updatePercep = true;

	if(liz->getStamina() < 25)
		return FSM_STATE_LIZARD_GO_HOME;

	if(liz->nearestLizard != NULL){
		Point2d matepos = liz->nearestLizard->getDrawable()->getCentre();
		float dm = matepos.dist(lizPos);
		if(dm > sq(320))
			return FSM_STATE_LIZARD_WANDER;
		if(!liz->nearestLizard->Isbreeding() && (liz->nearestLizard->getBreedingDelay() == 0 && liz->getBreedingDelay() == 0) && liz->getHunger() < 225 && liz->nearestLizard->getHunger() < 225){
			if(dm < liz->getDrawable()->getHeight()/2 * liz->getDrawable()->getHeight()/2)
				return FSM_STATE_LIZARD_MATE;
		}else{
			return FSM_STATE_LIZARD_WANDER;
		}
	}
	else
		return FSM_STATE_LIZARD_WANDER;

	return type;
}
