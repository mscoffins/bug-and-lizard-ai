#include "FSMStateBugWander.h"
#include <random>
#include <time.h>
#include "AIActorBug.h"


FSMStateBugWander::FSMStateBugWander(int state, AIActor* p)
{
	this->type = state;
	this->parent = p;
}

void FSMStateBugWander::update(float deltaT)
{
	AIActorBug* bug = (AIActorBug*)parent;

	int x,y;
	Point2d pos = bug->getDrawable()->getCentre();
	x = ((int)pos.x + worldW/2)/cellsize;
	y = ((int)pos.y + worldH/2)/cellsize;
	
	if(path.size() == 0){
		int tx,ty;
		if(bug->nearestBug != NULL && pos.dist(bug->nearestBug->getDrawable()->getCentre()) > sq(720)){
			tx = ((int)bug->nearestBug->getDrawable()->getCentre().x + worldW/2)/cellsize;
			ty = ((int)bug->nearestBug->getDrawable()->getCentre().y + worldH/2)/cellsize;
		}else{
			if(pos.dist(bug->nearestFood->getPos()) > sq(720)){
				tx = ((int)bug->nearestFood->getPos().x + worldW/2)/cellsize;
				ty = ((int)bug->nearestFood->getPos().y + worldH/2)/cellsize;
			}else{		
				tx = x + (rand()%8 - 4);
				ty = y + (rand()%8 - 4);
			}
		}
		while(tx < 0 || tx > mapWidth-1 || ty < 0 || ty > mapHeight-1 || (ty == y && tx == x) || parent->map[ty * mapWidth + tx] == -1){
			tx = x + (rand()%8 - 4);
			ty = y + (rand()%8 - 4);
		}
		AStar pathfinder(parent->map);
		path = pathfinder.search(Point2d(y,x),Point2d(ty,tx));
		if(path[0].x == y && path[0].y == x){
			path[0].x--;
			path[0].y--;
		}
	}
	
	float px,py;
	px = path.back().y * cellsize - worldW/2 + cellsize/2;
	py = path.back().x * cellsize - worldH/2 + cellsize/2; 
	int angle =  (atan2(py - pos.y,px - pos.x) * 180) / PI;
	int bAngle = bug->getAngle();
	bug->setAngle(angle);
	if(path.back().y == x && path.back().x == y){
		path.pop_back();
	}
}

void FSMStateBugWander::enter()
{
	path = vector<Point2d>();
	parent->path = &path;
}

void FSMStateBugWander::exit()
{
	parent->updatePercep = true;
	parent->path = NULL;
}

void FSMStateBugWander::init()
{
	path = vector<Point2d>();
}

int FSMStateBugWander::checkTransitions() {
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d bugPos = parent->getDrawable()->getCentre();

	if(bug->nearestLiz != NULL){
		Point2d lizPos = bug->nearestLiz->getDrawable()->getCentre();
		float lizr = bug->nearestLiz->getDrawable()->getHeight();
		float dl = lizPos.dist(bugPos);
		if(dl <= (lizr * lizr))
			return FSM_STATE_BUG_RUN_AWAY;
	}else
		bug->updatePercep = true;
	
	if(bug->nearestFood != NULL){
		Point2d foodPos = bug->nearestFood->getPos();
		float df = foodPos.dist(bugPos);
		if(bug->getHunger() >= 10 && bug->nearestFood->foodVal > 10 && df <= sq(160))
			return FSM_STATE_BUG_EAT_FOOD;
	}else
		bug->updatePercep = true;

	if(bug->getStamina() < 25)
		return FSM_STATE_BUG_GO_HOME;
	
	if(bug->nearestBug != NULL){
		AIActorBug* mate = bug->nearestBug;
		Point2d matePos = bug->nearestBug->getDrawable()->getCentre();
		float dm = matePos.dist(bugPos);
		if(dm < sq(240) && !mate->Isbreeding() && !bug->Isbreeding() && (mate->getBreedingDelay() == 0 && bug->getBreedingDelay() == 0) && (mate->getAge() >= 20 && bug->getAge() >= 20) && bug->getHunger() < 80 && mate->getHunger() < 80)
			return FSM_STATE_BUG_FIND_MATE;
	}else
		bug->updatePercep = true;

	return type;
}

