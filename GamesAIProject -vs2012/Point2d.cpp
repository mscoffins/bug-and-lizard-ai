#include "Point2d.h"
#include <math.h>
//default constuctor
Point2d::Point2d(){
	this->x = 0;
	this->y = 0;
}
//constructor
Point2d::Point2d(float x, float y){
	this->x = x;
	this->y = y;
}
//getters and setters
float Point2d::getX(){return x;}

float Point2d::getY(){return y;}

void Point2d::setX(float x){this->x = x;}

void Point2d::setY(float y){this->y = y;}

//normalise vector
void Point2d::normalise() {
	x = x / (sqrtf((x*x) + (y*y)));
	y = y / (sqrtf((x*x) + (y*y)));
}

//calc distance between 2 vectors
float Point2d::dist(Point2d p){
	return (x - p.x)*(x - p.x) + (y - p.y)*(y - p.y);
}
//operator overloads
Point2d Point2d::operator+(Point2d v){
	Point2d pos;
	pos.setX(this->x + v.getX());
	pos.setY(this->y + v.getY());
	return pos;
}

Point2d Point2d::operator-(Point2d v){
	Point2d pos;
	pos.setX(this->x - v.getX());
	pos.setY(this->y - v.getY());
	return pos;
}

Point2d Point2d::operator*(float v){
	Point2d pos;
	pos.setX(this->x * v);
	pos.setY(this->y * v);
	return pos;
}

Point2d Point2d::operator/(float v){
	Point2d pos;
	pos.setX(this->x / v);
	pos.setY(this->y / v);
	return pos;
}

inline bool operator==(Point2d a, Point2d b){
	if(a.x == b.x && a.y == b.y)
		return true;
	return false;
}