#include "Food.h"
int Food::count = 0;
//not used
Food::Food(GLuint t){
	count++;
	if(count > 100)
		count = 1;
	tex = t;
	maxSize = foodVal = rad;
	ttl = maxSize/3;
}

Food::Food(GLuint t, Point2d pos){
	count++;
	if(count > 100)
		count = 1;
	setPos(count, pos);
	//this->pos = pos;
	tex = t;
	maxSize = foodVal = rad;
	ttl = maxSize/3;
}

void Food::draw(){
	glPushMatrix();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glBindTexture(GL_TEXTURE_2D, tex);
	glBegin(GL_POLYGON);
	for(int i=0; i<360; i+=5)
	{
		float xcoord = pos.x + this->rad * cos(i*(3.141f/180));
		float ycoord = pos.y + this->rad * sin(i*(3.141f/180));
		glTexCoord2f(0.5 + 0.5 * cos(i*(3.141f/180)), 0.5 + 0.5 * sin(i*(3.141f/180))); 
		glVertex2f(xcoord, ycoord);
	}
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glPopMatrix();
}
//not used
void Food::setPos(int seed, Point2d pos){
	srand(time(NULL)+count*seed);
	rand();
	rad = 30 + rand()%60;
	this->pos = pos;
	foodVal = rad;
	ttl = maxSize/3;
}

void Food::update(float deltaT){
	ttl-= deltaT;
	if(ttl <= 0){	
		if(foodVal < maxSize){
			foodVal+= 0.2;
			rad = foodVal;
		}
	}
}