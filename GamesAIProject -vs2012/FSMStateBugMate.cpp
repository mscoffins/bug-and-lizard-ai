#include "FSMStateBugMate.h"

FSMStateBugMate::FSMStateBugMate(int state, AIActor* p)
{
	this->type = state;
	this->parent = p;
	mateDur = 0;
}

void FSMStateBugMate::update(float deltaT)
{
	mateDur += deltaT;
}

void FSMStateBugMate::enter()
{
	mateDur = 0;
	AIActorBug* bug = (AIActorBug*)parent;
	if(bug->nearestBug != NULL){
		AIActorBug* mate = bug->nearestBug;
		bug->setSpeed(0);
		mate->setSpeed(0);
		mate->setBreeding(true);
		bug->setBreeding(true);
	}

}

void FSMStateBugMate::exit()
{
	AIActorBug* bug = (AIActorBug*)parent;
	if(bug->nearestBug != NULL){
		AIActorBug* mate = bug->nearestBug;
		mate->setSpeed(mate->baseSpeed);
		bug->setSpeed(bug->baseSpeed);
		bug->setBreeding(false);
		mate->setBreeding(false);

		if(mateDur >= 2){
			bug->childFlag = true;
			bug->setBreedingDelay(bug->baseBreedingDelay);
			mate->setBreedingDelay(mate->baseBreedingDelay);
		}
	}
	bug->updatePercep = true;
}

void FSMStateBugMate::init()
{
}

int FSMStateBugMate::checkTransitions() {
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d bugPos = parent->getDrawable()->getCentre();
	if(mateDur >= 2 || bug->nearestBug == NULL)
		return FSM_STATE_BUG_WANDER;
	if(bug->nearestLiz != NULL){
		Point2d lizPos = bug->nearestLiz->getDrawable()->getCentre();
		float lizr = bug->nearestLiz->getDrawable()->getHeight();
		float dl = lizPos.dist(bugPos);
		if(dl <= (lizr * lizr))
			return FSM_STATE_BUG_RUN_AWAY;
	}else
		bug->updatePercep = true;

	return type;
}