#include "FSMStateLizardEatBug.h"

FSMStateLizardEatBug::FSMStateLizardEatBug(int state, AIActor* p)
{
	type = state;
	parent = p;
	eatDur = 0;
}

void FSMStateLizardEatBug::update(float deltaT)
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	eatDur += deltaT;
	liz->setHunger(liz->getHunger() - 150*deltaT);
	liz->setStamina(liz->getStamina() + 5*deltaT);
	liz->getDrawable()->setWidth(liz->getDrawable()->getWidth() + deltaT);
	liz->getDrawable()->setHeight(liz->getDrawable()->getHeight() + deltaT*2);
}

void FSMStateLizardEatBug::enter()
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	liz->nearestBug->setCaught();
	lizSpeed = parent->getSpeed();
	parent->setSpeed(0);
	eatDur = 0;
}

void FSMStateLizardEatBug::exit()
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	liz->nearestBug->kill();
	liz->nearestBug->causeOfDeath = "Eaten";
	parent->setSpeed(lizSpeed);
	liz->updatePercep = true;
}

void FSMStateLizardEatBug::init()
{
}

int FSMStateLizardEatBug::checkTransitions()
{
	if(eatDur > 2)
		return FSM_STATE_LIZARD_WANDER;

	return type;
}