#include "Home.h"

int Home::currentId = 0;

Home::Home(vector<GLuint> textures, HomeType t){
	id = currentId++;
	type = t;
	resedents = 0;
	if(type == HomeType::BUG_HOME)
		home = new Drawable(100,50,Point2d(rand()%4800 - 2400,rand()%3840 - 1920),&textures);		
	else
		home = new Drawable(150,150,Point2d(rand()%4800 - 2400,rand()%3840 - 1920),&textures);
	
	angle = rand()%360;

	home->init();

}

Home::Home(vector<GLuint> textures, HomeType type, Point2d pos){
	id = currentId++;
	this->type = type;
	resedents = 0;
	if(type == HomeType::BUG_HOME)
		home = new Drawable(100,50,pos,&textures);		
	else
		home = new Drawable(150,150,pos,&textures);
	
	angle = rand()%360;

	home->init();
}

void Home::draw(){
	home->draw(angle);
}
	
void Home::init(){
	//home.init();
}