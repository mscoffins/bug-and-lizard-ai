#pragma once

#include "Drawable.h"
//#include "AIActor.h"
enum HomeType{BUG_HOME, LIZARD_HOME};

class AIActor;

class Home{	
	
private:
	Drawable* home;
	HomeType type;
	float angle;
public:
	Home(vector<GLuint> textures, HomeType type);
	Home(vector<GLuint> textures, HomeType type, Point2d pos);
	void draw();
	void init();
	Drawable* getDrawable(){return home;}

	int resedents;
	int id;
	static int currentId;
};