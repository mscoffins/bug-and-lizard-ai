#pragma once
#include "FSMState.h"
#include "AIActor.h"
#include "AIActorBug.h"
#include <random>
#include <time.h>

class FSMStateBugWander : public FSMState {
private:
	vector<Point2d> path;
	float resetDelay;
	float angleStep;
public:
	FSMStateBugWander(int state, AIActor* p);

	virtual void update(float deltaT);
	virtual void enter();
	virtual void exit();
	virtual void init();
	virtual int checkTransitions();
};