#include "FSMStateBugRunAway.h"

FSMStateBugRunAway::FSMStateBugRunAway(int s, AIActor* p){
	type = s;
	parent = p;
}
void FSMStateBugRunAway::update(float deltaT){

	AIActorBug* bug = (AIActorBug*)parent;
	Point2d pos = parent->getDrawable()->getCentre();
	float px,py;
	px = path.back().y * cellsize - worldW/2 + cellsize/2;
	py = path.back().x * cellsize - worldH/2 + cellsize/2; 
	float angle =  (atan2(py - pos.y,px - pos.x) * 180) / PI;
	bug->setAngle(angle);
	if(sq(pos.x - px) < 16 && sq(pos.y - py) < 16 && path.size() > 1){
		path.pop_back();
	}
	//srand(time(NULL) + bug->id);
	//rand();
	//float r = rand() / (float)RAND_MAX - 0.5;
	//freq_r += deltaT;
	//if (freq_r >= 0.01){
	//	//parent->setAngle(180/3.141f * atan2f(lizpos.y - bugpos.y,lizpos.x - bugpos.x)-(180-10*r));
	//	parent->setAngle(parent->getAngle() + 10*r);
	//	freq_r = 0;
	//}

	
}
void FSMStateBugRunAway::enter(){
	path = vector<Point2d>();
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d pos = parent->getDrawable()->getCentre();
	Point2d lizpos = bug->nearestLiz->getDrawable()->getCentre();
	Point2d homepos = bug->nearestHome->getDrawable()->getCentre();
	//parent->setAngle(180/3.141f * atan2f(lizpos.y - bugpos.y,lizpos.x - bugpos.x)-180);
	int bx,by,tx,ty,lx,ly;
	bx = ((int)pos.x + worldW/2)/cellsize;
	by = ((int)pos.y + worldH/2)/cellsize;
	tx = (homepos.x + worldW/2)/cellsize;
	ty = (homepos.y + worldH/2)/cellsize;
	lx = ((int)lizpos.x + worldW/2)/cellsize;
	ly = ((int)lizpos.y + worldH/2)/cellsize;
	//bug->map[ly * mapWidth + lx] = -1;
	AStar pathfinder(bug->map);
	path = pathfinder.search(Point2d(by,bx),Point2d(ty,tx));
	//bug->map[ly * mapWidth + lx] = 1;
	parent->setSpeed(parent->baseSpeed + 40);
	parent->path = &path;
	
}
void FSMStateBugRunAway::exit(){
	parent->setSpeed(parent->baseSpeed);
	parent->updatePercep = true;
	parent->path = NULL;
}
void FSMStateBugRunAway::init(){
	path = vector<Point2d>();
}

int FSMStateBugRunAway::checkTransitions(){
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d bugpos = parent->getDrawable()->getCentre();

	if(bug->nearestLiz == NULL)
		return FSM_STATE_BUG_WANDER;
	else
		bug->updatePercep = true;

	Point2d lizpos = bug->nearestLiz->getDrawable()->getCentre();
	float lizr = bug->nearestLiz->getDrawable()->getHeight();
	float d = lizpos.dist(bugpos);
	if(d > sq(160))
		return FSM_STATE_BUG_WANDER;

	int bx = ((int)bugpos.x + worldW/2)/cellsize;
	int by = ((int)bugpos.y + worldH/2)/cellsize;
	float px = path[0].y * cellsize - worldW/2 + cellsize/2;
	float py = path[0].x * cellsize - worldH/2 + cellsize/2; 
	if(path.size() == 1 && bx == path[0].y && by == path[0].x && sq(bugpos.x - px) < 16 && sq(bugpos.y - py) < 16)
		return FSM_STATE_BUG_REST;

	return type;
}
