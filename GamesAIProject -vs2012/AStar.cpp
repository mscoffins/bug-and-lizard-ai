#include "AStar.h"
#include <time.h>

bool compareNodePointers(AStarNode* a, AStarNode* b){ return a->f > b->f; }

AStar::AStar(int* map){
	openList = std::vector<AStarNode*>();
	closedList = std::vector<AStarNode*>();
	mapNodes = std::vector<AStarNode*>(MAP_WIDTH*MAP_HEIGHT);
	this->map = map;
	//populate node array based on map
	for(int x = 0; x < MAP_HEIGHT; x++){
		for(int y = 0; y < MAP_WIDTH; y++){
			nodeStatus s = UNEXPLORED;
			int w = map[x*MAP_WIDTH + y];
			if( w == -1)
				s = INVALID;
			mapNodes[x*MAP_WIDTH + y] = new AStarNode(x,y,s,w);
		}
	}
	startNode = NULL;
	goalNode = NULL;
}
int AStar::huristic(AStarNode* pos){
	return 10 * (std::abs(goalNode->x - pos->x) + std::abs(goalNode->y - pos->y));
}
std::vector<Point2d> AStar::search(Point2d start, Point2d end){
	goalNode = mapNodes[(int)end.x * MAP_WIDTH + end.y];
	startNode = mapNodes[(int)start.x * MAP_WIDTH + start.y];
	startNode->g = 0;
	startNode->h = huristic(startNode);
	startNode->f = startNode->g+startNode->h;
	//AStarNode* p = startNode;
	startNode->status = OPENLIST;
	openList.push_back(startNode);
	std::make_heap(openList.begin(), openList.end(),compareNodePointers);
	while(openList.size() > 0){
		AStarNode* b = getbestNode();
		//AStarNode* b = openList.at(0);
		//reached goal node, bail out
		if(b->id == goalNode->id)
			return getPath(b);

		AStarNode temp;
		//process b's children
		//start at cell left and go round in clockwise loop
		//left
		temp = AStarNode(b->x,b->y-1);
		if(temp.y >= 0 && map[temp.x*MAP_WIDTH + temp.y] != -1)
			linkChild(b, temp, map[temp.id] * 10);
		//bot-left
		temp = AStarNode(b->x+1,b->y-1);
		if(map[(temp.x)*MAP_WIDTH + (temp.y+1)] != -1 && map[(temp.x-1)*MAP_WIDTH + temp.y] != -1){
			if((temp.y >= 0 && temp.x < MAP_HEIGHT) && map[temp.x*MAP_WIDTH + temp.y] != -1)
				linkChild(b, temp, map[temp.id] * 14);
		}
		//bot
		temp = AStarNode(b->x+1,b->y);
		if(temp.x < MAP_HEIGHT && map[temp.x*MAP_WIDTH + temp.y] != -1)
			linkChild(b, temp, map[temp.id] * 10);
		//bot-right
		temp = AStarNode(b->x+1,b->y+1);
		if(map[(temp.x-1)*MAP_WIDTH + (temp.y)] != -1 && map[(temp.x)*MAP_WIDTH + (temp.y-1)] != -1){
			if((temp.y < MAP_WIDTH && temp.x < MAP_HEIGHT) && map[temp.x*MAP_WIDTH + temp.y] != -1)
				linkChild(b, temp, map[temp.id] * 14);
		}
		//right
		temp = AStarNode(b->x,b->y+1);
		if(temp.y < MAP_WIDTH && map[temp.x*MAP_WIDTH + temp.y] != -1)
			linkChild(b, temp, map[temp.id] * 10);
		//top-right
		temp = AStarNode(b->x-1,b->y+1);
		if(map[(temp.x)*MAP_WIDTH + (temp.y-1)] != -1 && map[(temp.x+1)*MAP_WIDTH + temp.y] != -1){
			if((temp.y < MAP_WIDTH && temp.x >= 0) && map[temp.x*MAP_WIDTH + temp.y] != -1)
				linkChild(b, temp, map[temp.id] * 14);
		}
		//top
		temp = AStarNode(b->x-1,b->y);
		if(temp.x >= 0 && map[temp.x*MAP_WIDTH + temp.y] != -1)
			linkChild(b, temp, map[temp.id] * 10);
		//top-left
		temp = AStarNode(b->x-1,b->y-1);
		if(map[(temp.x)*MAP_WIDTH + (temp.y+1)] != -1 && map[(temp.x+1)*MAP_WIDTH + temp.y] != -1){
			if((temp.y >= 0 && temp.x >= 0) && map[temp.x*MAP_WIDTH + temp.y] != -1)
				linkChild(b, temp, map[temp.id] * 14);
		}
		closedList.push_back(b);
		b->status = CLOSEDLIST;
		//std::pop_heap(openList.begin(), openList.end(),compareNodePointers);
		//openList.pop_back();
		openList.erase(openList.begin());
		//std::make_heap(openList.begin(), openList.end(),compareNodePointers);
	}
	//no path found
	std::vector<Point2d> path = std::vector<Point2d>();
	path.push_back(start);
	return path;
}
AStarNode* AStar::getbestNode(){
	std::make_heap(openList.begin(), openList.end(),compareNodePointers);
	//std::sort(openList.begin(), openList.end());

	/*AStarNode* best = openList.at(0);
	for each(AStarNode* n in openList){
		if(n->f < best->f)
			best = n;
	}
	return best;*/
	return openList[0];
}

int AStar::getBestNodePos(int id){
	for(int i = 0; i < openList.size(); i++){
		if(openList.at(i)->id == id)
			return i;
	}
	return 0;
}

std::vector<Point2d> AStar::getPath(AStarNode* end){
	std::vector<Point2d> path = std::vector<Point2d>();
	path.push_back(Point2d(end->x,end->y));
	if(end->id == startNode->id)
		return path;
	AStarNode* current = end->parent;
	while(current->id != startNode->id){
		path.push_back(Point2d(current->x, current->y));
		current = current->parent;
	}
	return path;
}

void AStar::linkChild(AStarNode* parent, AStarNode temp, int weight){

	int x = temp.x;
	int y = temp.y;
	int g = parent->g + weight;
	int id = temp.id;

	AStarNode* check = NULL;

	if(mapNodes[id]->status == OPENLIST){
		check = mapNodes[id];
		parent->children[parent->numChildren++] = check;

		if(g < check->g){
			check->parent = parent;
			check->g = g;
			check->f = g + check->h;
		}
	}
	else if(mapNodes[id]->status == CLOSEDLIST){
		check = mapNodes[id];
		parent->children[parent->numChildren++] = check;

		if(g < check->g){
			check->parent = parent;
			check->g = g;
			check->f = g + check->h;

			updateParents(check);
		}
	}
	else{
		AStarNode* node = mapNodes[id];
		node->parent = parent;
		node->g = g;
		node->h = huristic(node);
		node->f = node->g + node->h;
		node->status = OPENLIST;
		
		//if(!std::is_heap(openList.begin(), openList.end(),compareNodePointers))
			//std::make_heap(openList.begin(), openList.end(),compareNodePointers);
		openList.push_back(node);
		//std::push_heap(openList.begin(), openList.end(),compareNodePointers);

		parent->children[parent->numChildren++] = node;
	}
}

void AStar::updateParents(AStarNode* node){
	int g = node->g;
	int c = node->numChildren;

	AStarNode* child = NULL;
	std::vector<AStarNode*> stack = std::vector<AStarNode*>();
	for(int i = 0; i < c; i++){
		child = node->children[i];
		if(g+1 < child->g){
			child->g = g+1;
			child->f = child->g + child->h;
			child->parent = node;

			stack.push_back(child);
		}
	}

	AStarNode* parent;
	while(stack.size() > 0){
		parent = stack.back();
		stack.pop_back();
		c = parent->numChildren;

		for(int i = 0; i < c; i++){
			child = parent->children[i];
			
			if(parent->g+1 < child->g){
				int mapVal = child->g - child->parent->g;
				int w = 10;
				int newW = 10;

				if(child->parent->x != child->x && child->parent->y != child->y)
					w = 14;

				if(parent->x != child->x && parent->y != child->y)
					newW = 14;

				mapVal /= w;
				child->g = parent->g + w * mapVal;
				child->f = child->g + child->h;
				child->parent = parent;

				stack.push_back(child);
			}
		}
	}
}

AStarNode* AStar::checkList(std::vector<AStarNode*> list, int id){
	for each(AStarNode* n in list){
		if(n->id == id){
			return n;
		}
	}
	return NULL;
}

void AStar::draw(){
	for(int x = 0; x < MAP_HEIGHT; x++){
		std::cout << std::endl;
		for(int y = 0; y < MAP_WIDTH; y++){
			bool inPath = false;
			if(x == startNode->x && y == startNode->y)
				std::cout << "S";
			else
				if(x == goalNode->x && y == goalNode->y)
					std::cout << "E";
				else{
					if(mapNodes[x * MAP_WIDTH + y]->status == OPENLIST)
						std::cout << "O";
					if(mapNodes[x * MAP_WIDTH + y]->status == CLOSEDLIST)
						std::cout << "C";
					if(mapNodes[x * MAP_WIDTH + y]->status == INVALID)
						std::cout << "0";
					if(mapNodes[x * MAP_WIDTH + y]->status == UNEXPLORED)
						std::cout << "1";
				}

		}
	}
	std::cout << "\n\n" << std::endl;
}