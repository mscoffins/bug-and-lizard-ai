#pragma once

#include "FSMState.h"
#include "AIActor.h"
#include "AIActorBug.h"
#include <random>
#include <time.h>

class FSMStateBugMate : public FSMState{
private:
	float mateDur;
public:
	FSMStateBugMate(int state, AIActor* p);

	virtual void update(float deltaT);
	virtual void enter();
	virtual void exit();
	virtual void init();
	virtual int checkTransitions();
};