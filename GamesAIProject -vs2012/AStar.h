#pragma once

#include "AStarNode.h"
#include "Point2d.h"
#include <vector>
#include <algorithm>
#include <hash_set>
#include <iostream>
class AStar{
private: 
	std::vector<AStarNode*> openList;
	std::vector<AStarNode*> closedList;
	AStarNode* startNode;
	AStarNode* goalNode;
	std::vector<AStarNode*> mapNodes;
	int* map;


public:
	AStar(int* map = nullptr);
	int huristic(AStarNode* pos);
	std::vector<Point2d> search(Point2d start, Point2d end);
	AStarNode* getbestNode();
	std::vector<Point2d> getPath(AStarNode* end);
	void linkChild(AStarNode* parent, AStarNode temp, int weight);
	void updateParents(AStarNode* node);
	AStarNode* checkList(std::vector<AStarNode*> list, int id);
	int getBestNodePos(int id);
	void draw();
	~AStar() {
		while (!mapNodes.empty()) {
			AStarNode* n = mapNodes.back();
			mapNodes.pop_back();
			delete n;
		}
	}
};