
//includes areas for keyboard control, mouse control, resizing the window

#include "glew.h"
#include <windows.h>		// Header file for Windows
#include <gl\gl.h>			// Header file for the OpenGL32 Library
#include <gl\glu.h>			// Header file for the GLu32 Library

#include "General.h"

#include "console.h"		// Header file for Console
#include "nvImage.h"
#include "GameWorld.h"
#include "TextureLib.h"
#include "Statistics.h"

#include "glfont2.h"

glfont::GLFont myFont;

ConsoleWindow console;		//console window variable
//deltaT variables
clock_t currentTime = clock();
clock_t prevTime = clock();
double deltaT = 0;
float speed;
//zoom level
const int VIEW_SIZE = 20;
float screenWidth = 1200;
float screenHeight = 900;
//inputs
bool keys[256];
bool leftPressed = false;
Point2d mousePos;
Point2d mouseWorldPos;
//textures and objects
TextureLib textures;
GameWorld world;
Statistics stats;
//OPENGL FUNCTION PROTOTYPES
void display();				//called in winmain to draw everything to the screen
void reshape(int width, int height);				//called when the window is resized
void init();				//called in winmain when the program starts.
void processKeys(double deltaT);         //called in winmain to process keyboard input
void update(double deltaT);				//called in winmain to update variables
GLuint loadPNG(char* name);

/*************    START OF OPENGL FUNCTIONS   ****************/
void display()									
{
	glClear(GL_COLOR_BUFFER_BIT);

	glViewport(screenWidth/4,0,screenWidth,screenHeight);						// Reset the current viewport

	glMatrixMode(GL_PROJECTION);						// select the projection matrix stack
	glLoadIdentity();									// reset the top of the projection matrix to an identity matrix

	glOrtho((-screenWidth/2) * world.zoom,(screenWidth/2) * world.zoom,(-screenHeight/2) * world.zoom,(screenHeight/2) * world.zoom,-10,10);           // set the coordinate system for the window

	glMatrixMode(GL_MODELVIEW);							// Select the modelview matrix stack
	glLoadIdentity();
	world.draw();
	glViewport(0,0,screenWidth/4,screenHeight);						// Reset the current viewport

	glMatrixMode(GL_PROJECTION);						// select the projection matrix stack
	glLoadIdentity();									// reset the top of the projection matrix to an identity matrix

	gluOrtho2D(0,200,0,750);           // set the coordinate system for the window

	glMatrixMode(GL_MODELVIEW);							// Select the modelview matrix stack
	glLoadIdentity();
	stats.draw();
	glFlush();
}

void update(double deltaT)
{
	//if(deltaT > 0.03)//cap deltaT
		//deltaT = 0.03;
	//float nmouse_x = ((float)mousePos.x-screenWidth/2.0);
	//float nmouse_y = -((float)mousePos.y-screenHeight/2.0);
	//l.setCentre(Point2d(nmouse_x,nmouse_y));
	world.update(deltaT);
	stats.update(deltaT);
	//cout<<"x = " << nmouse_x << ", y = " << nmouse_y << endl;
	
}

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth=width; screenHeight = height;           // to ensure the mouse coordinates match 
	world.screenW = width;													// we will use these values to set the coordinate system

	glViewport(width/4,0,width,height);						// Reset the current viewport

	glMatrixMode(GL_PROJECTION);						// select the projection matrix stack
	glLoadIdentity();									// reset the top of the projection matrix to an identity matrix

	glOrtho((-width/2) * world.zoom,(width/2) * world.zoom,(-height/2) * world.zoom,(height/2) * world.zoom,-10,10);           // set the coordinate system for the window

	glMatrixMode(GL_MODELVIEW);							// Select the modelview matrix stack
	glLoadIdentity();									// Reset the top of the modelview matrix to an identity matrix
}


void init()
{
	glClearColor(0.55,0.65,0.23,0.0);
	if(!myFont.Create("Arial.glf",1))
		exit(0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	textures.init();
	world = GameWorld(&textures);
	world.init();
	stats = Statistics(&textures,&world,&myFont);
	stats.init();

	speed = 1;

}
void processKeys(double deltaT)
{
	if(keys['0']){
		if(speed < 10)
			speed += 0.2;
		keys['0'] = false;
	}
	if(keys['9']){
		if(speed > 1)
			speed -= 0.2;
		keys['9'] = false;
	}
	//cout << world.cameraPos.x << ", " << world.cameraPos.y << endl;
	if(keys[VK_UP]){
		if(stats.mode == EDIT_PARAMETERS){
			if(stats.selection > 1)
				stats.selection--;
			keys[VK_UP] = false;
		}
		else{
			if((world.cameraPos.y + (world.zoom*screenHeight)/2) < worldH/2)
				world.cameraPos.y += 200*deltaT;
		}
	}
	if(keys[VK_DOWN]){
		if(stats.mode == EDIT_PARAMETERS){
			if(stats.selection < 12)
				stats.selection++;
			keys[VK_DOWN] = false;
		}
		else{
			if((world.cameraPos.y - (world.zoom*screenHeight)/2) > -worldH/2)
				world.cameraPos.y -= 200*deltaT;
		}
	}
	if(keys[VK_LEFT]){
		if(stats.mode == EDIT_PARAMETERS){
			stats.editParamDec();
			keys[VK_LEFT] = false;
		}
		else{
			if(world.cameraPos.x + world.zoom *(screenWidth/2) < worldW/2)
				world.cameraPos.x += 200*deltaT;
		}
	}
	if(keys[VK_RIGHT]){
		if(stats.mode == EDIT_PARAMETERS){
			stats.editParamInc();
			keys[VK_RIGHT] = false;
		}
		else{
			if(world.cameraPos.x - world.zoom*(screenWidth/4)  > -worldW/2)
				world.cameraPos.x -= 200*deltaT;
		}
	}
	if(keys['L'])
		world.goToNearestLizard();
	if(keys['B'])
		world.goToNearestBug();
	if(keys['Z']){
		if(world.zoom < 4.5){ 
			world.zoom += 0.05;
			//glMatrixMode(GL_PROJECTION);						// select the projection matrix stack
			//glLoadIdentity();									// reset the top of the projection matrix to an identity matrix
			//glOrtho((-screenWidth/2) * world.zoom,(screenWidth/2) * world.zoom,(-screenHeight/2) * world.zoom,(screenHeight/2) * world.zoom,-10,10);
			//glMatrixMode(GL_MODELVIEW);							// Select the modelview matrix stack
			//glLoadIdentity();									// Reset the top of the modelview matrix to an identity matrix
			
			keys['Z'] = false;
		}
	}
	if(keys['X']){
		if(world.zoom > 0.3){ 
			world.zoom -= 0.05;
			//glMatrixMode(GL_PROJECTION);						// select the projection matrix stack
			//glLoadIdentity();									// reset the top of the projection matrix to an identity matrix
			//glOrtho((-screenWidth/2) * world.zoom,(screenWidth/2) * world.zoom,(-screenHeight/2) * world.zoom,(screenHeight/2) * world.zoom,-10,10);
			//glMatrixMode(GL_MODELVIEW);							// Select the modelview matrix stack
			//glLoadIdentity();
			keys['X'] = false;
		}
	}
	if(keys['P']){
		cout << "Bugs:" << world.bugCount << "     Lizards: " << world.lizCount << endl;
		keys['P'] = false;
	}
	if(keys['F']){
		world.followMode = !world.followMode;
		keys['F'] = false;
	}
	if(keys['W']){
		world.currentLizard++;
		keys['W'] = false;
	}

	if(keys['1'])
		stats.mode = POP_GRAPHS;
	if(keys['2'])
		stats.mode = HEAT_MAPS_POP;
	if(keys['3'])
		stats.mode = HEAT_MAPS_DEATH;
	if(keys['4'])
		stats.mode = TARGET_STATS;
	if(keys['5'])
		stats.mode = EDIT_PARAMETERS;

	if(keys[VK_SPACE]){
		if(stats.mode == EDIT_PARAMETERS){
			world.reset();
			stats.init();
		}
		keys[VK_SPACE] = false;
	}
	//handle mouse positional movement
	//split world region into 3x3 grid
	if(mousePos.x > screenWidth/4){
		if(mousePos.y < screenHeight/3){//top section
			if(mousePos.x > 3*screenWidth/4){//right
				if(world.cameraPos.x - world.zoom*(screenWidth/4)  > -worldW/2)
					world.cameraPos.x -= 200*deltaT/speed;
				if((world.cameraPos.y + (world.zoom*screenHeight)/2) < worldH/2)
					world.cameraPos.y += 200*deltaT/speed;
				}
			if(mousePos.x > screenWidth/2 && mousePos.x < 3*screenWidth/4){//middle
				if((world.cameraPos.y + (world.zoom*screenHeight)/2) < worldH/2)
					world.cameraPos.y += 200*deltaT/speed;
			}
			if(mousePos.x < screenWidth/2){//left
				if(world.cameraPos.x + world.zoom *(screenWidth/2) < worldW/2)
					world.cameraPos.x += 200*deltaT/speed;
				if((world.cameraPos.y + (world.zoom*screenHeight)/2) < worldH/2)
					world.cameraPos.y += 200*deltaT/speed;
			}
		}
		if(mousePos.y > screenHeight/3 && mousePos.y < 2*screenHeight/3){//middle section
			if(mousePos.x > 3*screenWidth/4){//right
				if(world.cameraPos.x - world.zoom*(screenWidth/4)  > -worldW/2)
					world.cameraPos.x -= 200*deltaT/speed;
			}
			if(mousePos.x < screenWidth/2){//left
				if(world.cameraPos.x + world.zoom *(screenWidth/2) < worldW/2)
					world.cameraPos.x += 200*deltaT/speed;
			}
		}
		if(mousePos.y > 2*screenHeight/3){//bottom section
			if(mousePos.x > 3*screenWidth/4){//right
				if(world.cameraPos.x - world.zoom*(screenWidth/4)  > -worldW/2)
					world.cameraPos.x -= 200*deltaT/speed;
				if((world.cameraPos.y - (world.zoom*screenHeight)/2) > -worldH/2)
					world.cameraPos.y -= 200*deltaT/speed;
			}
			if(mousePos.x > screenWidth/2 && mousePos.x < 3*screenWidth/4){//middle
				if((world.cameraPos.y - (world.zoom*screenHeight)/2) > -worldH/2)
					world.cameraPos.y -= 200*deltaT/speed;
			}
			if(mousePos.x < screenWidth/2){//left
				if(world.cameraPos.x + world.zoom *(screenWidth/2) < worldW/2)
					world.cameraPos.x += 200*deltaT/speed;
				if((world.cameraPos.y - (world.zoom*screenHeight)/2) > -worldH/2)
					world.cameraPos.y -= 200*deltaT/speed;
			}
		}
	}
	
}


/**************** END OPENGL FUNCTIONS *************************/

//WIN32 functions
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
void KillGLWindow();									// releases and destroys the window
bool CreateGLWindow(char* title, int width, int height); //creates the window
int WINAPI WinMain(	HINSTANCE, HINSTANCE, LPSTR, int);  // Win32 main function

//win32 global variabless
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application


/******************* WIN32 FUNCTIONS ***************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	MSG		msg;									// Windows Message Structure
	bool	done=false;								// Bool Variable To Exit Loop


	console.Open();

	// Create Our OpenGL Window
	if (!CreateGLWindow("Game AI bug and lizard",screenWidth,screenHeight))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{

			// TIMER
			// Get the current time
			currentTime = clock();
			clock_t clockTicksTaken = currentTime - prevTime;
			deltaT =(clockTicksTaken / (double) CLOCKS_PER_SEC);
			deltaT *= speed;

			if (keys[VK_ESCAPE])
				done = true;
				
			processKeys(deltaT);			//process keyboard

			update(deltaT);					// update variables

						// Advance timer
			prevTime = currentTime;					
			

			display();					// Draw The Scene
			
			SwapBuffers(hDC);				// Swap Buffers (Double Buffering)
		}
	}

	console.Close();

	// Shutdown
	KillGLWindow();									// Kill The Window
	return (int)(msg.wParam);						// Exit The Program
}

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		break;

		case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			prevTime = clock();
			return 0;								// Jump Back
		}
		break;

		case WM_LBUTTONDOWN:
			{
				world.getClosestAI(mouseWorldPos);
			}
		break;

		case WM_LBUTTONUP:
			{
			}
		break;

		case WM_MOUSEMOVE:
			{
				mousePos.x = LOWORD(lParam);
				mousePos.y = HIWORD(lParam);
				mouseWorldPos.x = world.cameraPos.x - (mousePos.x - 3*screenWidth/4) * world.zoom;
				mouseWorldPos.y = world.cameraPos.y - (mousePos.y - screenHeight/2)*world.zoom; 
				
			}
		break;
		case WM_MOUSEWHEEL:
			{
				
				float deltaWheel = HIWORD(wParam) / (100*WHEEL_DELTA);
				deltaWheel -= 2.5;
				if(deltaWheel < 0){
					if(world.zoom > 0.3)
						world.zoom += (deltaWheel/50);
				}else{
					if(world.zoom < 4.5)
						world.zoom += (deltaWheel/50);
				}
				
			}
		break;
		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		break;
		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void KillGLWindow()								// Properly Kill The Window
{
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
 *	title			- Title To Appear At The Top Of The Window				*
 *	width			- Width Of The GL Window Or Fullscreen Mode				*
 *	height			- Height Of The GL Window Or Fullscreen Mode			*/
 
bool CreateGLWindow(char* title, int width, int height)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}
	
	dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
	dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	
	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								"OpenGL",							// Class Name
								title,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								hInstance,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(hRC=wglCreateContext(hDC)))				// Are We Able To Get A Rendering Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!wglMakeCurrent(hDC,hRC))					// Try To Activate The Rendering Context
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Activate The GL Rendering Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	reshape(width, height);					// Set Up Our Perspective GL Screen

	init();
	
	return true;									// Success
}



