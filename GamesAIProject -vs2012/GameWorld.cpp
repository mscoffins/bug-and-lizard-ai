#include "GameWorld.h"

GameWorld::GameWorld(TextureLib* tl){
	worldWidth = worldW;
	worldHeight = worldH;
	maxBugs = 200;
	maxLizards = 50;
	maxFood = 50;
	lizards = vector<AIActorLizard*>(25);
	bugs = vector<AIActorBug*>(100);
	food = vector<Food*>(50);
	bugHomes = vector<Home*>(10);
	lizHomes = vector<Home*>(5);
	texLib = tl;
	cameraPos = Point2d();
	zoom = 4.1;
	walls = vector<Drawable*>(50);
	currentBug = 0;
	currentLizard = 0;
	followMode = false;
	target = nullptr;
	screenW = 1200;

	ifstream bugParams ("bugParams.txt");
	if (bugParams.is_open()){
		bugParams >> bugStartProperties;
		bugParams.close();
    }else
		bugStartProperties = AIProperties(100,1,3,1.75,110,15);
	
	ifstream lizParams ("lizParams.txt");
	if (lizParams.is_open()){
		lizParams >> lizStartProperties;
		lizParams.close();
    }else
		lizStartProperties = AIProperties(190,1,2,9,130,20);

	minBugs = 60;
	minLizBugRatio = 7;
}

void GameWorld::init(){
	srand(time(NULL));
	Point2d pos;
	float x;
	float y;
	Gender g;
	vector<GLuint>* texs;
	//allows the initialisation of array for map
	for(int i = 0; i < mapWidth*mapHeight; i++){
		objMap[i] = 0;
		bugMap[i] = 1;
		lizMap[i] = 1;
		bugPopMap[i] = 0;
		bugDeathMap[i] = 0;
		lizPopMap[i] = 0;
		lizDeathMap[i] = 0;
	}

	bugDeaths[0] = bugDeaths[1] = bugDeaths[2] = 0;
	lizDeaths[0] = lizDeaths[1] = 0;

	for(int i = 0; i < 50; i++){
		int x = rand()%mapWidth;
		int y = rand()%mapHeight;
		while(objMap[x * mapHeight + y] == -1){
			x = rand()%mapWidth;
			y = rand()%mapHeight;
		}
		
		Drawable* w = new Drawable(160,160,Point2d((x * 160 + 80)-worldWidth/2,(y*160 + 80) - worldHeight/2),&texLib->wallTexs);
		w->init();
		walls[i] = (w);
		
		objMap[x * mapHeight + y] = -1;
		lizMap[y * mapWidth + x] = -1;
		bugMap[y * mapWidth + x] = -1;

		x = rand()%mapWidth;
		y = rand()%mapHeight;
		while(objMap[x * mapHeight + y] == -1){
			x = rand()%mapWidth;
			y = rand()%mapHeight;
		}
		//create food
		//food.push_back(new Food(texLib->foodtex, Point2d((x * 160 + 80) - worldWidth/2,(y * 160 + 80)- worldHeight/2)));//ignore
		objMap[x * mapHeight + y] = -1;
		food[i] = (new Food(texLib->foodtex, Point2d((x * 160 + 80) - worldWidth/2,(y * 160 + 80)- worldHeight/2)));
		
	}

	for(int i = 0; i < 10; i++){
		int x = rand()%mapWidth;
		int y = rand()%mapHeight;
		while(objMap[x * mapHeight + y] == -1){
			x = rand()%mapWidth;
			y = rand()%mapHeight;
		}
		objMap[x * mapHeight + y] = -1;
		//bugHomes.push_back(new Home(texLib->bugHomeTexs,BUG_HOME,Point2d((x*160 + 80) - worldWidth/2,(y*160 + 80) - worldHeight/2)));//ignore
		bugHomes[i] = (new Home(texLib->bugHomeTexs,BUG_HOME,Point2d((x*160 + 80) - worldWidth/2,(y*160 + 80) - worldHeight/2)));
		lizMap[y * mapWidth + x] = -1;
		if(i%2 == 0){
			while(objMap[x * mapHeight + y] == -1){
				x = rand()%mapWidth;
				y = rand()%mapHeight;
			}
			objMap[x * mapHeight + y] = -1;
			//lizHomes.push_back(new Home(texLib->lizHomeTexs,LIZARD_HOME,Point2d((x*160 + 80) - worldWidth/2,(y*160 + 80) - worldHeight/2)));//ignore
			lizHomes[i/2] = (new Home(texLib->lizHomeTexs,LIZARD_HOME,Point2d((x*160 + 80) - worldWidth/2,(y*160 + 80) - worldHeight/2)));
			bugMap[y * mapWidth + x] = -1;
		}
	}

	for(int i = 0; i < 100; i++){
		rand();
		x = bugHomes.at(rand()%10)->getDrawable()->getCentre().x;
		y = bugHomes.at(rand()%10)->getDrawable()->getCentre().y;
		pos = Point2d(x,y);
		g = Gender(rand()%2);
		if(g == MALE)
			texs = &texLib->bugtexsM;
		else
			texs = &texLib->bugtexsF;
		Drawable* b = new Drawable(25,25,pos,texs);
		b->init();
		//bugs.push_back(new AIActorBug(b,g));//create bug//ignore
		bugs[i] = (new AIActorBug(b,g,bugMap,bugStartProperties));

	}

	for(int i = 0; i < 25; i++){
			//create lizard
			x = lizHomes.at(rand()%5)->getDrawable()->getCentre().x;
			y = lizHomes.at(rand()%5)->getDrawable()->getCentre().y;
			pos = Point2d(x,y);
			g = Gender(rand()%2);
			if(g == MALE)
				texs = &texLib->liztexsM;
			else
				texs = &texLib->liztexsF;
			Drawable* l = new Drawable(50,100,pos,texs);
			l->init();
			//lizards.push_back(new AIActorLizard(l,g));//ignore
			lizards[i] = (new AIActorLizard(l,g, lizMap,lizStartProperties));
		}

	

	for each(AIActorBug* bug in bugs){
		bug->init();
		bug->nearestHome = getClosestHome(bug,bugHomes);
	}
	
	for each(AIActorLizard* liz in lizards){
		liz->init();
		liz->nearestHome = getClosestHome(liz,lizHomes);
	}

	for each(Home* h in bugHomes)
		h->init();
	for each(Home* h in lizHomes)
		h->init();
}

void GameWorld::update(float deltaT){
	//clear population maps
	for(int i = 0; i <  mapHeight*mapWidth; i++){
		bugPopMap[i] = 0;
		lizPopMap[i] = 0;
	}
	//clear resedents in each home
	for each(Home* h in bugHomes)
		h->resedents = 0;
	for each(Home* h in lizHomes)
		h->resedents = 0;
	//update food sources
	for each(Food* f in food){
		f->update(deltaT);
	}
	//create list for new bugs to be introduced this frame
	vector<AIActorBug*> babyBugs = vector<AIActorBug*>();
	//vector for gender textures
	vector<GLuint>* texs;
	//gender type variable
	Gender g;
	//loop over all bugs, updating them as neccessary
	for each(AIActorBug* bug in bugs){
		//only update them if they are alive
		if(bug->isAlive()){
			//get world position, calculate grid position and increment that bin in population map
			Point2d pos = bug->getDrawable()->getCentre();
			int x = ((int)pos.x + worldW/2)/cellsize;
			int y = ((int)pos.y + worldH/2)/cellsize;
			bugPopMap[x*mapHeight + y]++;
			//if the bug has finished mating and has its child flag set, process the child flag
			if(bug->childFlag){
				//generate between 2-4 baby bugs
				for(int i = 0; i < rand()%3 + 2; i++){
					AIProperties p = bug->getPropeties(); //get parent properties
					//if(bug->nearestBug != NULL)	//both parent properties
						//p = p.avg(bug->nearestBug->getPropeties());
					g = Gender(rand()%2); //random gender
					//assign appropriate texture set
					if(g == MALE)
						texs = &texLib->bugtexsM;
					else
						texs = &texLib->bugtexsF;
					//create drawable object for bug
					Drawable* b = new Drawable(25,25,bug->getDrawable()->getCentre(),texs);
					b->init();
					//add baby bug to baby bug list
					babyBugs.push_back(new AIActorBug(b,g, bugMap,bugStartProperties));
					//increment population histogram
					bugPopMap[x*mapHeight + y]++;
				}
				bug->childFlag = false;
			}
			//update bugs perception data if flag is set
			if(bug->updatePercep){
				bug->updatePerceptions(getClosestLizard(bug),getClosestBug(bug),getClosestFood(bug), getClosestHome(bug,bugHomes));
				bug->updatePercep = false;
			}
			//increment home resedency
			bug->nearestHome->resedents++;
			//call bug update function
			bug->update(deltaT);
		}
	}

	//remove dead bugs
	vector<AIActorBug*>::iterator it = bugs.begin();
	while (it != bugs.end()) {
		if (isDead(*it)) {
			it = bugs.erase(it);
		}
		else {
			++it;
		}
	}
	//maintain minimum population of bugs
	if(bugs.size() < minBugs){		
		g = Gender(rand()%2);
		if(g == MALE)
			texs = &texLib->bugtexsM;
		else
			texs = &texLib->bugtexsF;
		Drawable* b = new Drawable(25,25,Point2d(rand()%worldWidth - worldWidth/2,rand()%worldHeight - worldHeight/2),texs);
		b->init();
		babyBugs.push_back(new AIActorBug(b,g,bugMap,bugStartProperties));
	}
	//add baby bugs to the scene with respect to max population
	for each(AIActorBug* b in babyBugs){
		if(bugs.size() < maxBugs){
			b->nearestHome = getClosestHome(b,bugHomes);
			b->init();
			bugs.push_back(b);
		}
	}

	//create list for baby lizards to be added
	vector<AIActorLizard*> babyLizs = vector<AIActorLizard*>();
	//process all lizards
	for each(AIActorLizard* liz in lizards){
		//only update is alive
		if(liz->isAlive()){
			//get position and calculate current grid square and incremnet population histogram
			Point2d pos = liz->getDrawable()->getCentre();
			int x = ((int)pos.x + worldW/2)/cellsize;
			int y = ((int)pos.y + worldH/2)/cellsize;
			lizPopMap[x*mapHeight + y]++;
			//process child flag
			if(liz->childFlag){
				//parent properties
				AIProperties p = liz->getPropeties();
				//if(liz->nearestLizard != NULL)	//properties of both parents
					//p = p.avg(liz->nearestLizard->getPropeties());
				//create 1-2 baby lizards
				for(int i = 0; i <= rand()%2; i++){
					//random gender
					g = Gender(rand()%2);
					//set appropriate textures
					if(g == MALE)
						texs = &texLib->liztexsM;
					else
						texs = &texLib->liztexsF;
					//create drawable object for lizard
					Drawable* l = new Drawable(50,100,liz->getDrawable()->getCentre(),texs);
					l->init();
					//add baby lizard to new lizards list
					babyLizs.push_back(new AIActorLizard(l,g,lizMap,lizStartProperties));
					//increment population count
					lizPopMap[x*mapHeight + y]++;
				}
				//disable child falg
				liz->childFlag = false;
			}
			//update lizards perception data if flag is set 
			if(liz->updatePercep){
				liz->updatePerceptions(getClosestBug(liz), getClosestLizard(liz), getClosestHome(liz,lizHomes));
				liz->updatePercep = false;
			}
			//increment home resedents
			liz->nearestHome->resedents++;
			//call lizard update function
			liz->update(deltaT);
		}
	}
	

	//remove dead lizards
	vector<AIActorLizard*>::iterator it2 = lizards.begin();
	while (it2 != lizards.end()) {
		if (isDead(*it2)) {
			it2 = lizards.erase(it2);
		}
		else {
			++it2;
		}
	}
	//maintain base lizard population relative to bug population
	if(lizards.size() < bugs.size()/minLizBugRatio){	
		g = Gender(rand()%2);
		if(g == MALE)
			texs = &texLib->liztexsM;
		else
			texs = &texLib->liztexsF;
		Drawable* l = new Drawable(50,100,Point2d(rand()%worldWidth - worldWidth/2,rand()%worldHeight - worldHeight/2),texs);
		l->init();
		babyLizs.push_back(new AIActorLizard(l,g,lizMap,lizStartProperties));		
	}
	//add all new baby lizards to the scene
	for each(AIActorLizard* l in babyLizs){
		if(lizards.size() < maxLizards){
			l->nearestHome = getClosestHome(l,lizHomes);
			l->init();
			lizards.push_back(l);
		}
	}

}


void GameWorld::draw(){
	glLoadIdentity();
	if(followMode){
		if(target != NULL){
			cameraPos = target->getDrawable()->getCentre();
			cameraPos.x -= (screenW/8) * zoom;
		}
	}
	
	gluLookAt(cameraPos.getX(),cameraPos.getY(),-1,cameraPos.getX(),cameraPos.getY(),0,0,1,0);
	glPushMatrix();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texLib->background);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex2f(-worldWidth, -worldHeight);
	glTexCoord2f(48.0, 0.0); glVertex2f(worldWidth, -worldHeight);
	glTexCoord2f(48.0, 48.0); glVertex2f(worldWidth, worldHeight);
	glTexCoord2f(0.0, 48.0); glVertex2f(-worldWidth, worldHeight);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glPopMatrix();
	for each(Food* f in food){
		f->draw();
	}
	for each(Drawable* w in walls){
		w->draw(0);
	}

	if(target != NULL){
		if(target->isAlive()){
			glPushMatrix();
			glEnable(GL_BLEND);
			glColor4f(0,0.5,1,0.5);
			glBegin(GL_POLYGON);
			for(int i=0; i<360; i+=5)
			{
				float xcoord = target->getDrawable()->getCentre().x + (target->getDrawable()->getHeight()/2) * cos(i*(3.141f/180));
				float ycoord = target->getDrawable()->getCentre().y + (target->getDrawable()->getHeight()/2) * sin(i*(3.141f/180));
				glVertex2f(xcoord, ycoord);
			}
			glEnd();
			glDisable(GL_BLEND);
			glPopMatrix();

			if(target->path != NULL){
				glColor3f(1,0.5,0);
				glLineWidth(2);
				glBegin(GL_LINE_STRIP);
				for each(Point2d p in *target->path){
					glVertex2f(p.y * cellsize - worldW/2 + cellsize/2,p.x * cellsize - worldH/2 + cellsize/2);
				}
				glVertex2f(target->getDrawable()->getCentre().x, target->getDrawable()->getCentre().y);
				glEnd();
			}
		}
	}

	for each(AIActorBug* bug in bugs){
		if(bug->isAlive()){
			bug->draw();
		}
	}
	for each(AIActorLizard* liz in lizards){
		if(liz->isAlive()){
			liz->draw();
		}
	}
	for each(Home* h in bugHomes)
		h->draw();
	for each(Home* h in lizHomes)
		h->draw();
	
}
AIActorLizard* GameWorld::getClosestLizard(AIActorBug* bug){
	AIActorLizard* closest = NULL;
	Point2d lizPos;
	Point2d bugPos = bug->getDrawable()->getCentre();
	float bestD = INT_MAX;
	for each(AIActorLizard* liz in lizards){
		if(liz->isAlive()){
			lizPos = liz->getDrawable()->getCentre();
			float d = bugPos.dist(lizPos);
			if(d < bestD){
				closest = liz;
				bestD = d;
			}
		}
	}
	return closest;
}
AIActorLizard* GameWorld::getClosestLizard(AIActorLizard* liz){
	AIActorLizard* closest = NULL;
	Point2d lizPos;
	Point2d lPos = liz->getDrawable()->getCentre();
	float bestD = INT_MAX;
	for each(AIActorLizard* lizard in lizards){
		if(liz->id != lizard->id  && lizard->isAlive() && lizard->sex != liz->sex){
			lizPos = lizard->getDrawable()->getCentre();
			float d = lPos.dist(lizPos);
			if(d < bestD){
				closest = lizard;
				bestD = d;
			}
		}
	}
	return closest;
}
AIActorBug* GameWorld::getClosestBug(AIActorLizard* liz){
	AIActorBug* closest = NULL;
	Point2d bugPos;
	Point2d lizPos = liz->getDrawable()->getCentre();
	float bestD = INT_MAX;
	for each(AIActorBug* bug in bugs){
		if(bug->isAlive()){
			bugPos = bug->getDrawable()->getCentre();
			float d = lizPos.dist(bugPos);
			if(d < bestD && !bug->atHome){
				closest = bug;
				bestD = d;
			}
		}
	}
	return closest;
}
AIActorBug* GameWorld::getClosestBug(AIActorBug* bug){
	AIActorBug* closest = NULL;
	Point2d bPos;
	Point2d bugPos = bug->getDrawable()->getCentre();
	float bestD = INT_MAX;
	for each(AIActorBug* b in bugs){
		if(bug->id != b->id && b->isAlive() && b->sex != bug->sex){
			bPos = b->getDrawable()->getCentre();
			float d = bugPos.dist(bPos);
			if(d < bestD){
				closest = b;
				bestD = d;
			}
		}
	}
	return closest;
}
Food* GameWorld::getClosestFood(AIActorBug* bug){
	Food* closest = NULL;
	Point2d foodPos;
	Point2d bugPos = bug->getDrawable()->getCentre();
	float bestD = INT_MAX;
	for each(Food* f in food){
		if(f->getRad() > 0){
			foodPos = f->getPos();
			float d = bugPos.dist(foodPos);
			if(d < bestD){
				closest = f;
				bestD = d;
			}
		}
	}
	return closest;
}

Home* GameWorld::getClosestHome(AIActor* ai, vector<Home*> homes){
	Home* closest = NULL;
	Point2d homePos;
	Point2d aiPos = ai->getDrawable()->getCentre();
	float bestD = INT_MAX;
	for each(Home* h in homes){
		homePos = h->getDrawable()->getCentre();
		float d = aiPos.dist(homePos);
		if(d < bestD && h->resedents < 25){
			closest = h;
			bestD = d;
		}
	}
	return closest;
}

void GameWorld::goToNearestBug(){
	Point2d bugPos = cameraPos;
	float bestD = INT_MAX;
	for each(AIActorBug* b in bugs){
		float d = cameraPos.dist(b->getDrawable()->getCentre());
		if(d < bestD && b->isAlive()){
			bestD = d;
			bugPos = b->getDrawable()->getCentre();
		}
	}
	cameraPos = bugPos;
}

void GameWorld::goToNearestLizard(){
	Point2d lizPos = cameraPos;
	float bestD = INT_MAX;
	for each(AIActorLizard* l in lizards){
		float d = cameraPos.dist(l->getDrawable()->getCentre());
		if(d < bestD  && l->isAlive()){
			bestD = d;
			lizPos = l->getDrawable()->getCentre();
		}
	}
	cameraPos = lizPos;
}

bool GameWorld::isDead(AIActorBug* bug){
	if(!bug->isAlive()){
		cout << "Bug: " << bug->causeOfDeath << endl;
		Point2d pos = bug->getDrawable()->getCentre();
		int x = ((int)pos.x + worldW/2)/cellsize;
		int y = ((int)pos.y + worldH/2)/cellsize;
		bugDeathMap[x * mapHeight + y]++;
		if(bug->causeOfDeath == "Old Age")
			bugDeaths[0]++;
		if(bug->causeOfDeath == "Hunger")
			bugDeaths[1]++;
		if(bug->causeOfDeath == "Eaten")
			bugDeaths[2]++;
	}
	return !bug->isAlive();
}
bool GameWorld::isDead(AIActorLizard* liz){
	if(!liz->isAlive()){
		cout << "Lizard: " << liz->causeOfDeath << endl;
		Point2d pos = liz->getDrawable()->getCentre();
		int x = ((int)pos.x + worldW/2)/cellsize;
		int y = ((int)pos.y + worldH/2)/cellsize;
		lizDeathMap[x * mapHeight + y]++;
		if(liz->causeOfDeath == "Old Age")
			lizDeaths[0]++;
		if(liz->causeOfDeath == "Hunger")
			lizDeaths[1]++;
	}
	return !liz->isAlive();
}

void GameWorld::getClosestAI(Point2d pos){
	AIActor* closest = bugs[0];
	float lowestDist = pos.dist(closest->getDrawable()->getCentre());
	float tempDist;
	for each(AIActor* b in bugs){
		tempDist = pos.dist(b->getDrawable()->getCentre());
		if(lowestDist > tempDist){
			lowestDist = tempDist;
			closest = b;
		}
	}
	for each(AIActor* l in lizards){
		tempDist = pos.dist(l->getDrawable()->getCentre());
		if(lowestDist > tempDist){
			lowestDist = tempDist;
			closest = l;
		}
	}
	
	target = closest;
}

void GameWorld::reset(){
	vector<AIActorBug*>::iterator it = bugs.begin();
	while (it != bugs.end()) {
		it = bugs.erase(it);
	}
	vector<AIActorLizard*>::iterator it2 = lizards.begin();
	while (it2 != lizards.end()) {
		it2 = lizards.erase(it2);
	}
	vector<Home*>::iterator it3 = bugHomes.begin();
	while (it3 != bugHomes.end()) {
		it3 = bugHomes.erase(it3);
	}
	vector<Home*>::iterator it4 = lizHomes.begin();
	while (it4 != lizHomes.end()) {
		it4 = lizHomes.erase(it4);
	}
	vector<Drawable*>::iterator it5 = walls.begin();
	while (it5 != walls.end()) {
		it5 = walls.erase(it5);
	}
	vector<Food*>::iterator it6 = food.begin();
	while (it6 != food.end()) {
		it6 = food.erase(it6);
	}

	lizards = vector<AIActorLizard*>(25);
	bugs = vector<AIActorBug*>(100);
	food = vector<Food*>(50);
	bugHomes = vector<Home*>(10);
	lizHomes = vector<Home*>(5);
	walls = vector<Drawable*>(50);

	ofstream bugParams("bugParams.txt", ios::out | ios::trunc);
	ofstream lizParams("lizParams.txt", ios::out | ios::trunc);

	bugParams << bugStartProperties;
	lizParams << lizStartProperties;

	init();
}