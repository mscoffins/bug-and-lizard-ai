#include "FSMStateBugFindMate.h"

FSMStateBugFindMate::FSMStateBugFindMate(int s, AIActor* p){
	type = s;
	parent = p;
}
void FSMStateBugFindMate::update(float deltaT){

	AIActorBug* bug = (AIActorBug*)parent;
	Point2d bugpos = parent->getDrawable()->getCentre();
	if(bug->nearestBug != NULL){
		Point2d matepos = bug->nearestBug->getDrawable()->getCentre();
		parent->setAngle(180/3.141f * atan2f(matepos.y - bugpos.y,matepos.x - bugpos.x));
	}
}
void FSMStateBugFindMate::enter(){
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d bugpos = parent->getDrawable()->getCentre();
	Point2d matepos = bug->nearestBug->getDrawable()->getCentre();
	parent->setAngle(180/3.141f * atan2f(matepos.y - bugpos.y,matepos.x - bugpos.x));
}
void FSMStateBugFindMate::exit(){
	parent->updatePercep = true;
}
void FSMStateBugFindMate::init(){

}

int FSMStateBugFindMate::checkTransitions(){
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d bugPos = parent->getDrawable()->getCentre();
	
	if(bug->nearestLiz != NULL){
		Point2d lizPos = bug->nearestLiz->getDrawable()->getCentre();
		float lizr = bug->nearestLiz->getDrawable()->getHeight();
		float dl = lizPos.dist(bugPos);
		if(dl <= (lizr * lizr))
				return FSM_STATE_BUG_RUN_AWAY;
	}else
		bug->updatePercep = true;

	if(bug->nearestFood != NULL){
		Point2d foodPos = bug->nearestFood->getPos();
		float df = foodPos.dist(bugPos);
		if(bug->getHunger() >= 10 && bug->nearestFood->foodVal > 10 && df <= ((bug->nearestFood->getRad()*2 + parent->getDrawable()->getHeight()) * (bug->nearestFood->getRad()*2 + parent->getDrawable()->getHeight())))
			return FSM_STATE_BUG_EAT_FOOD;
	}else
		bug->updatePercep = true;

	if(bug->getStamina() < 20)
		return FSM_STATE_BUG_GO_HOME;

	if(bug->nearestBug != NULL){
		Point2d matePos = bug->nearestBug->getDrawable()->getCentre();
		float dm = matePos.dist(bugPos);
		if(dm < sq(bug->getDrawable()->getHeight()/2) && !bug->nearestBug->Isbreeding() && (bug->nearestBug->getBreedingDelay() == 0 && bug->getBreedingDelay() == 0) && bug->getHunger() < 90 && bug->nearestBug->getHunger() < 90)
			return FSM_STATE_BUG_MATE;
		else{
			if(dm > sq(320))
				return FSM_STATE_BUG_WANDER;
			return type;
		}
	}
	else
		return FSM_STATE_BUG_WANDER;

	return type;
}
