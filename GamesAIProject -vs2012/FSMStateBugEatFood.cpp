#include "FSMStateBugEatFood.h"
#include <random>
#include <time.h>
#include <math.h>
#include "AIActorBug.h"

FSMStateBugEatFood::FSMStateBugEatFood(int s, AIActor* p){
	type = s;
	parent = p;
	bugSpeed = 0;
}

void FSMStateBugEatFood::update(float deltaT)
{
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d foodP = bug->nearestFood->getPos();
	Point2d bugPos = parent->getDrawable()->getCentre();

	float d = foodP.dist(bugPos);
	if(d <= ((bug->nearestFood->getRad() + parent->getDrawable()->getHeight()/2)*(bug->nearestFood->getRad() + parent->getDrawable()->getHeight()/2))){
		if(parent->getSpeed() !=0)
			parent->setSpeed(0);
		bug->nearestFood->ttl = bug->nearestFood->maxSize/3;
		bug->nearestFood->foodVal -= deltaT * 5;
		bug->nearestFood->setRad(bug->nearestFood->foodVal);
		bug->setHunger(bug->getHunger() - deltaT*5);
		bug->setStamina(bug->getStamina() + 10*deltaT);
		bug->getDrawable()->setHeight(bug->getDrawable()->getHeight() + deltaT*2);
		bug->getDrawable()->setWidth(bug->getDrawable()->getWidth() + deltaT*2);
	}
	else
		bug->setSpeed(bugSpeed);
}

void FSMStateBugEatFood::enter()
{
	AIActorBug* bug = (AIActorBug*)parent;
	foodPos = bug->nearestFood->getPos();
	Point2d bugPos = parent->getDrawable()->getCentre();

	bugSpeed = parent->getSpeed();
	parent->setAngle(180/3.141f*atan2f(foodPos.y - bugPos.y,foodPos.x - bugPos.x));
}

void FSMStateBugEatFood::exit()
{
	AIActorBug* bug = (AIActorBug*)parent;
	parent->setSpeed(bugSpeed);
	bug->updatePercep = true;
}

void FSMStateBugEatFood::init()
{
}

int FSMStateBugEatFood::checkTransitions() {
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d bugPos = parent->getDrawable()->getCentre();
	if(bug->nearestLiz != NULL){
		Point2d lizPos = bug->nearestLiz->getDrawable()->getCentre();
		float lizr = bug->nearestLiz->getDrawable()->getHeight();
		float dl = lizPos.dist(bugPos);
		if(dl <= (lizr * lizr))
			return FSM_STATE_BUG_RUN_AWAY;
	}else
		bug->updatePercep = true;

	if(bug->getHunger() <= 0 || bug->nearestFood->foodVal <= 0 || (foodPos.x != bug->nearestFood->getPos().x && foodPos.y != bug->nearestFood->getPos().y))
		return FSM_STATE_BUG_WANDER;
	return type;
}