#pragma once
#include "AStar.h"
#include <string>
using namespace std;
class AIActor;


enum states {
	FSM_STATE_NONE, FSM_STATE_BUG_WANDER, FSM_STATE_BUG_EAT_FOOD, FSM_STATE_BUG_RUN_AWAY,FSM_STATE_BUG_GO_HOME, FSM_STATE_BUG_REST, FSM_STATE_BUG_FIND_MATE, FSM_STATE_BUG_MATE,
	FSM_STATE_LIZARD_WANDER, FSM_STATE_LIZARD_CHASE_BUG, FSM_STATE_LIZARD_EAT_BUG, FSM_STATE_LIZARD_GO_HOME, FSM_STATE_LIZARD_REST, FSM_STATE_LIZARD_FIND_MATE, FSM_STATE_LIZARD_MATE, FSM_STATE_LIZARD_REST_AWAY
};

class FSMState {

public:
	FSMState(int state = FSM_STATE_NONE);

	virtual void update(float deltaT);
	virtual void enter();
	virtual void exit();
	virtual void init();
	virtual int checkTransitions();

	int type;
	AIActor* parent;
};
