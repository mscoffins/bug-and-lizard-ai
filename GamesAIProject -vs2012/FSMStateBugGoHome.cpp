#include "FSMStateBugGoHome.h"

FSMStateBugGoHome::FSMStateBugGoHome(int state, AIActor* p)
{
	this->type = state;
	this->parent = p;	
}

void FSMStateBugGoHome::update(float deltaT)
{
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d pos = parent->getDrawable()->getCentre();
	float px,py;
	int bx,by;
	px = path.back().y * cellsize - worldW/2 + cellsize/2;
	py = path.back().x * cellsize - worldH/2 + cellsize/2; 
	bx = (pos.x + worldW/2)/cellsize;
	by = (pos.y + worldH/2)/cellsize;
	if(sq(px - bx) > 1 || sq(py - by) > 1){
		enter();
		px = path.back().y * cellsize - worldW/2 + cellsize/2;
		py = path.back().x * cellsize - worldH/2 + cellsize/2; 
		bx = (pos.x + worldW/2)/cellsize;
		by = (pos.x + worldH/2)/cellsize;
	}
	float angle =  (atan2(py - pos.y,px - pos.x) * 180) / PI;
	float bAngle = bug->getAngle();
	float angleInc = (angle - bAngle);
	//if(sq(angle - bAngle) > 100)
	//	bug->setAngle(bug->getAngle() + angleInc);
	//else
		bug->setAngle(angle);

	//if(sq(pos.x - px) < 16 && sq(pos.y - py) < 16 && path.size() > 1){
	if(path.back().y == bx && path.back().x == by && path.size() > 1){
		path.pop_back();
	}
	
}

void FSMStateBugGoHome::enter()
{
	path.clear();
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d pos = parent->getDrawable()->getCentre();
	Point2d homepos = bug->nearestHome->getDrawable()->getCentre();
	int x = ((int)pos.x + 2400)/160;
	int y = ((int)pos.y + 1920)/160;
	int tx = (homepos.x + 2400)/160;
	int ty = (homepos.y + 1920)/160;
	if(x != tx || y != ty){
		AStar pathfinder(parent->map);
		path = pathfinder.search(Point2d(y,x),Point2d(ty,tx));
	}else{
		
		path.push_back(Point2d(ty,tx));
	}
	//parent->setAngle(180/3.141f * atan2f(homepos.y - bugpos.y,homepos.x - bugpos.x));
	parent->path = &path;
}

void FSMStateBugGoHome::exit()
{
	parent->updatePercep = true;
	parent->path = NULL;
}

void FSMStateBugGoHome::init()
{
	path = vector<Point2d>();
}

int FSMStateBugGoHome::checkTransitions() {
	AIActorBug* bug = (AIActorBug*)parent;
	Point2d bugPos = parent->getDrawable()->getCentre();
	Point2d homePos = bug->nearestHome->getDrawable()->getCentre();


	if(bug->nearestLiz != NULL){
		Point2d lizPos = bug->nearestLiz->getDrawable()->getCentre();
		float lizr = bug->nearestLiz->getDrawable()->getHeight();
		float dl = lizPos.dist(bugPos);
		if(dl <= (lizr * lizr))
				return FSM_STATE_BUG_RUN_AWAY;
	}else
		bug->updatePercep = true;

	if(bug->nearestFood != NULL){
		Point2d foodPos = bug->nearestFood->getPos();
		float df = foodPos.dist(bugPos);
		if(bug->getHunger() >= 20 && bug->nearestFood->foodVal > 10 && df <= ((bug->nearestFood->getRad()*2 + parent->getDrawable()->getHeight()) * (bug->nearestFood->getRad()*2 + parent->getDrawable()->getHeight())))
			return FSM_STATE_BUG_EAT_FOOD;
	}else
		bug->updatePercep = true;

	float dh = homePos.dist(bugPos);
	if(dh < 50)
		return FSM_STATE_BUG_REST;

	return type;
}
