#pragma once
#include "AIActor.h"
#include "AIActorBug.h"
#include "States.h"

class AIActorBug;

class AIActorLizard : public AIActor{
private:
	
public:
	static int CurrentID;

	AIActorLizard(){}
	AIActorLizard(Drawable* liz, Gender s, int* map);
	AIActorLizard(Drawable* liz, Gender s, int* map, AIProperties p);
	virtual void draw();
	virtual void update(float deltaT);
	virtual void init();
	void updatePerceptions(AIActorBug* nearBug, AIActorLizard* nearLiz, Home* nearHome);
	void spawnAtHome();

	AIActorLizard* nearestLizard;
	AIActorBug* nearestBug;
	float percepConf;
	int awayRests;

	~AIActorLizard(){
		delete brain;
	}
};