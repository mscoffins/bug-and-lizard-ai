#include "FSMStateLizardChaseBug.h"
#include <iostream>

FSMStateLizardChaseBug::FSMStateLizardChaseBug(int state, AIActor* p)
{
	type = state;
	parent = p;
}

void FSMStateLizardChaseBug::update(float deltaT)
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	Point2d lizpos = parent->getDrawable()->getCentre();
	if(liz->nearestBug != NULL){
		Point2d bugpos = liz->nearestBug->getDrawable()->getCentre();
		if(!liz->nearestBug->caught)
			parent->setAngle(180/3.141f * atan2f(bugpos.y - lizpos.y,bugpos.x - lizpos.x));

	}
}

void FSMStateLizardChaseBug::enter()
{
}

void FSMStateLizardChaseBug::exit()
{
	parent->updatePercep = true;
}

void FSMStateLizardChaseBug::init()
{
}

int FSMStateLizardChaseBug::checkTransitions()
{
	AIActorLizard* liz = (AIActorLizard*)parent;
	Point2d lizpos = parent->getDrawable()->getCentre();

	if(liz->nearestBug != NULL){
		Point2d bugpos = liz->nearestBug->getDrawable()->getCentre();
		int bx = ((int)bugpos.x + worldW/2)/cellsize;
		int by = ((int)bugpos.y + worldH/2)/cellsize;
		float lizr = parent->getDrawable()->getHeight();
		float d = bugpos.dist(lizpos);
		if(d > sq(320) || liz->nearestBug->caught || liz->map[by * mapWidth + bx] == -1)
			return FSM_STATE_LIZARD_WANDER;

		float bugr = liz->nearestBug->getDrawable()->getHeight();
		if(d <= sq(lizr/2) && !liz->nearestBug->caught)
			return FSM_STATE_LIZARD_EAT_BUG;
	}else
		liz->updatePercep = true;

	return type;
}